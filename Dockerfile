FROM centos/python-34-centos7:3.4

EXPOSE 8080 59999

# Install yum packages with the ROOT user
USER root

ENV LD_LIBRARY_PATH /opt/rh/rh-python34/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64
ENV PYTHONPATH /opt/app-root/src
ENV FLASK_APP /opt/app-root/src/wsgi.py

# Set the permissions for the app-user user
RUN chgrp -R 0 /opt/app-root && chmod -R ug+rwx /opt/app-root

USER 1001

WORKDIR /opt/app-root/src

RUN pip install --upgrade pip

# Install pip requirements
COPY requirements.txt /opt/app-root/src/requirements.txt
COPY ./requirements-dev.txt /opt/app-root/src/requirements-dev.txt

RUN pip install -r requirements.txt
RUN pip install -r requirements-dev.txt

CMD ["python", "wsgi.py"]