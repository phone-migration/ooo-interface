import os
import json
import responses
from flask import Flask
from flask_testing import TestCase
from flask_dance import OAuth2ConsumerBlueprint
from urlobject import URLObject
from flask_dance.consumer.backend import MemoryBackend

from flask import current_app, url_for
from httmock import urlmatch, response, HTTMock

from app.common.utils.cern_oauth import make_cern_blueprint, cern, cern_logged_in
from app.common.utils.logger import setup_webapp_logs
from app.extensions import db, cache
from app.users.models import User
from flask_login import (
    LoginManager
)

_basedir = os.path.abspath(os.path.dirname(__file__))

USER_DATA = {"name": "Burt Macklin Test",
             "username": "fernandor",
             "id": 12345678,
             "personid": 12345,
             "email": "burt.macklin@cern.ch",
             "first_name": "Burt",
             "last_name": "Macklin",
             "identityclass": "CERN Registered",
             "federation": "CERN",
             "phone": "+41902202122",
             "mobile": "null"
             }

GROUPS_DATA = {"groups": ["All Exchange People", "Users IT", "IT Web IT",
                          "GP Apply Favorites Redirection", "NICE Profile Redirection",
                          "GP Apply NoAdmin", "NICE MyDocuments Redirection (New)", "Bike-2-Work-Meyrin"]}


@urlmatch(netloc=r'(.*\.)?oauthresource\.web\.cern\.ch(.*)', path="/api/User")
def oauth_user_mock(url, request):
    current_app.logger.debug("Calling oauth mockup for user")
    headers = {'content-type': 'application/json'}

    return response(200, json.dumps(USER_DATA), headers, None, 5, request)


@urlmatch(netloc=r'(.*\.)?oauthresource\.web\.cern\.ch(.*)', path="/api/Groups")
def oauth_groups_mock(url, request):
    current_app.logger.debug("Calling oauth mockup for groups")
    headers = {'content-type': 'application/json'}

    return response(200, json.dumps(GROUPS_DATA), headers, None, 5, request)


class CernOauthTest(TestCase):
    def create_app(self):
        application = Flask(__name__)
        # setup_webapp_logs(application, to_file=False)

        return application

    def test_blueprint_factory(self):
        cern_bp = make_cern_blueprint(
            client_id="foo",
            client_secret="bar",
            redirect_to="index",
        )
        assert isinstance(cern_bp, OAuth2ConsumerBlueprint)
        assert cern_bp.session.client_id == "foo"
        assert cern_bp.client_secret == "bar"
        assert cern_bp.authorization_url == "https://oauth.web.cern.ch/OAuth/Authorize"
        assert cern_bp.token_url == "https://oauth.web.cern.ch/OAuth/Token"

    def test_load_from_config(self):
        self.app.config["CERN_OAUTH_CLIENT_ID"] = "foo"
        self.app.config["CERN_OAUTH_CLIENT_SECRET"] = "bar"
        self.app.config["TESTING"] = True
        self.app.secret_key = "anything"
        cern_bp = make_cern_blueprint(login_url="/cern")
        self.app.register_blueprint(cern_bp)

        resp = self.client.get(url_for('cern.login'))
        current_app.logger.debug(resp)
        url = resp.headers["Location"]
        client_id = URLObject(url).query.dict.get("client_id")
        assert client_id == "foo"

    @responses.activate
    def test_context_local(self):
        responses.add(responses.GET, "https://google.com")

        # set up two apps with two different set of auth tokens
        app1 = Flask(__name__)
        ghbp1 = make_cern_blueprint(
            "foo1", "bar1", redirect_to="url1",
            backend=MemoryBackend({"access_token": "app1"}),
        )
        app1.register_blueprint(ghbp1)

        app2 = Flask(__name__)
        ghbp2 = make_cern_blueprint(
            "foo2", "bar2", redirect_to="url2",
            backend=MemoryBackend({"access_token": "app2"}),
        )
        app2.register_blueprint(ghbp2)

        # outside of a request context, referencing functions on the `cern` object
        # will raise an exception
        with self.assertRaises(AttributeError):
            cern.get("https://google.com")

        # inside of a request context, `cern` should be a proxy to the correct
        # blueprint session
        with app1.test_request_context("/"):
            app1.preprocess_request()
            cern.get("https://google.com")
            request = responses.calls[0].request
            assert request.headers["Authorization"] == "Bearer app1"

        with app2.test_request_context("/"):
            app2.preprocess_request()
            cern.get("https://google.com")
            request = responses.calls[1].request
            assert request.headers["Authorization"] == "Bearer app2"

    def test_cern_logged_in(self):
        with HTTMock(oauth_user_mock):
            with HTTMock(oauth_groups_mock):
                app1 = Flask(__name__)
                app1.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')
                app1.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
                app1.config['CACHE_TYPE'] = "null"
                app1.config['DEBUG'] = True
                app1.config['TESTING'] = True
                app1.config['LOG_LEVEL'] = "DEV"
                app1.config['CERN_OAUTH_ADMIN_EGROUPS'] = "it-dep-cda-ic"
                app1.secret_key = "anything"

                ghbp1 = make_cern_blueprint(
                    "foo1", "bar1", redirect_to="url1",
                    backend=MemoryBackend({"access_token": "app1"}),
                )
                app1.register_blueprint(ghbp1)

                with app1.test_request_context("/"):
                    db.init_app(app1)
                    db.drop_all()
                    db.create_all()

                    cache.init_app(app1)
                    setup_webapp_logs(to_file=False)
                    app1.logger.debug("Logging setup")

                    login_manager = LoginManager()
                    login_manager.init_app(app1)

                    self.assertEqual(len(User.query.all()), 0)

                    app1.preprocess_request()
                    cern_logged_in(ghbp1, "1234")

                    self.assertEqual(len(User.query.all()), 1)

                    db.session.remove()
                    db.drop_all()
