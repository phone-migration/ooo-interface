import pytest
from click.testing import CliRunner
from flask.cli import ScriptInfo

from app.common.utils.cli import (init_db, initialize_cli, clear_db, init_office_phones, echo_command, init_office_resp,
                                  init_outside_resp)
from tests import BaseTestCase
from tests.common.app import app


class FlaskCliRunner(CliRunner):
    def __init__(self, app, **kwargs):
        super().__init__(**kwargs)
        self.app = app

    def invoke(self, cli=None, args=None, **kwargs):
        if cli is None:
            cli = self.app.cli
        if 'obj' not in kwargs:
            kwargs['obj'] = ScriptInfo(create_app=lambda _: self.app)
        return super().invoke(cli, args, **kwargs)


@pytest.fixture()
def cli_runner(application):
    yield FlaskCliRunner(application)


class TestCli(BaseTestCase):
    def test_initialize(self):
        initialize_cli(app)

    def test_init_db(self):
        runner = FlaskCliRunner(self.app)
        result = runner.invoke(init_db)
        print(result)
        self.assertEqual(result.exit_code, 0)

    def test_clear_db(self):
        runner = FlaskCliRunner(self.app)
        result = runner.invoke(clear_db)
        print(result)
        self.assertEqual(result.exit_code, 0)

    def test_init_phones(self):
        runner = FlaskCliRunner(self.app)
        result = runner.invoke(init_office_phones, ["--csv_file", "uploads/office_phones_small.csv"])
        print(result)
        self.assertEqual(result.exit_code, 0)

    def test_init_office_users(self):
        runner = FlaskCliRunner(self.app)
        self.assertEqual(self.app.config["OFFICE_MODE"], True)

        result = runner.invoke(init_office_resp, ["--csv_file", "uploads/office_users_small.csv"])
        print(result)
        self.assertEqual(result.exit_code, 0)

    def test_init_outside_users(self):
        runner = FlaskCliRunner(self.app)
        result = runner.invoke(init_outside_resp, ["--csv_file", "uploads/outside_users_small.csv"])
        print(result)
        self.assertEqual(result.exit_code, 0)

    def test_echo_command(self):
        runner = FlaskCliRunner(self.app)
        result = runner.invoke(echo_command)
        print(result)
        self.assertEqual(result.exit_code, 0)
