from app.common.utils.cli import initialize_cli
from tests import create_test_app

app = create_test_app()
initialize_cli(app)
