import os

_basedir = os.path.abspath(os.path.dirname(__file__))

TESTING = True
WTF_CSRF_ENABLED = False

SECRET_KEY = 'This string will be replaced with a proper key in production.'
APPLICATION_ROOT = "/"
CERN_OAUTH_CLIENT_ID = "test"
CERN_OAUTH_CLIENT_SECRET = "test"
CERN_OAUTH_TOKEN_URL = "/"
CERN_OAUTH_AUTHORIZE_URL = "/"

ADMIN_EGROUPS = ["it-dep-cda-ic"]

OFFICE_MODE = True

# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')
#
SQLALCHEMY_DATABASE_URI = 'memory:///'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_RECORD_QUERIES = False

APP_PORT = 8080
DEBUG = True
# Available values:
# DEV: Development purposes with DEBUG level
# PROD: Production purposes with INFO level
LOG_LEVEL = "PROD"
IS_LOCAL = True

# Needed to use Oauth
USE_PROXY = True

ANALYTICS_GOOGLE_ID = ""
ANALYTICS_PIWIK_ID = ""

#
# Database
#
DB_NAME = "test"
DB_PASS = "test"
DB_PORT = 1234
DB_SERVICE_NAME = "test"
DB_USER = "test"
APP_DB_ENGINE = None

SQLALCHEMY_POOL_SIZE = None
SQLALCHEMY_POOL_TIMEOUT = None
SQLALCHEMY_POOL_RECYCLE = None
SQLALCHEMY_MAX_OVERFLOW = None

#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = "test"
CACHE_REDIS_PORT = "1234"
CACHE_OAUTH_TIMEOUT = 300
CACHE_CONFIG = {'CACHE_TYPE': 'null'}

#
# PHONE API
#
PHONE_API_USER = ""
PHONE_API_PASS = ""

SEND_EMAIL = False
MAIL_HOSTNAME = ""
MAIL_FROM = ""
MAIL_TO = ""
WEBAPP_LOGS = "/opt/app-root/src/logs/test.log"

if os.environ.get('CI_PROJECT_DIR', None):
    TEMP_FILES_DEST = os.path.join(os.environ.get('CI_PROJECT_DIR'), 'web', 'tests', 'samples')
else:
    TEMP_FILES_DEST = os.path.join("/", "opt", "app-root", 'tests', 'samples')

FIM_API_USER = ""
FIM_API_PASSWORD = ""
FIM_API_ENDPOINT = "https://ucservices.web.cern.ch/api/phonenumbers"
