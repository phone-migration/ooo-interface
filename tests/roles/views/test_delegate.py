from flask_login import current_user

from app.common.exceptions import AppException
from app.extensions import db
from app.phone_numbers.models import ResponsibleUser
from flask import url_for

from app.roles.mappers import RoleLocationMapper, RoleDepartmentMapper, DelegatedUserMapper
from app.roles.models import DelegatedType, RoleLocation, RoleType, RoleDepartment
from tests import BaseTestCase


class DelegateViewsTest(BaseTestCase):

    def create_models(self):
        responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
                                      last_name="Macklin", first_name="Burt")
        existing_delegate = DelegatedUserMapper.create_delegated_user(name="not_admin",
                                                                      delegated_type=DelegatedType.USER,
                                                                      commit=True)
        db.session.add(responsible)

        role_location = RoleLocationMapper.create_role_location(role=RoleType.TSO,
                                                                building='1',
                                                                floor='1',
                                                                room='1', commit=True)
        role_location.delegate = existing_delegate
        existing_delegate.roles.append(role_location)

        role_dep = RoleDepartmentMapper.create_role_department(department="IT", commit=True)
        role_dep.delegate = existing_delegate
        existing_delegate.dep_roles.append(role_dep)

        db.session.commit()

    def test_delegate_reachable(self):
        with self.client:
            self.create_models()
            self.client.get(url_for('local_dev.login_admin'))
            role = RoleLocation.query.get(1)
            # Create the models: location, role_location, responsible
            self.assertTrue(current_user.is_admin)
            result = self.client.get(url_for('roles_bp.delegate', role_id=role.id))
            #
            self.assertEqual(result.status_code, 200)
            # self.assertIn("Review", str(result.data))

    def test_delegate_set_delegate(self):
        with self.client:
            self.create_models()
            self.client.get(url_for('local_dev.login_admin'))
            role = RoleLocation.query.get(1)
            # Create the models: location, role_location, responsible
            self.assertTrue(current_user.is_admin)
            data = {'username': 'fernandor',
                    'valid': 'true',
                    'type': 'user'
                    }
            result = self.client.post(url_for('roles_bp.delegate', role_id=role.id), data=data,
                                      follow_redirects=True)
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Delegate set successfully", str(result.data))

    def test_delegate_set_delegate_error(self):
        with self.client:
            self.create_models()
            self.client.get(url_for('local_dev.login_admin'))
            role = RoleLocation.query.get(1)
            # Create the models: location, role_location, responsible
            self.assertTrue(current_user.is_admin)
            data = {'username': 'fernandor',
                    'valid': 'true',
                    'type': 'INVENT'
                    }
            with self.assertRaises(AppException):
                result = self.client.post(url_for('roles_bp.delegate', role_id=role.id), data=data,
                                          follow_redirects=True)
                #
                self.assertEqual(result.status_code, 200)
                self.assertIn("Unable to set delegate", str(result.data))

    def test_delegate_set_delegate_outside_error2(self):
        with self.client:
            self.create_models()
            self.client.get(url_for('local_dev.login_admin'))
            self.app.config["OFFICE_MODE"] = False
            role = RoleLocation.query.get(1)
            # Create the models: location, role_location, responsible
            self.assertTrue(current_user.is_admin)
            data = {'username': 'fernandor',
                    'valid': 'amarillo',
                    'type': 'user'
                    }
            result = self.client.post(url_for('roles_bp.delegate', role_id=role.id), data=data,
                                      follow_redirects=True)
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Unable to set the delegate", str(result.data))

    def test_delegate_set_delegate_error2(self):
        with self.client:
            self.create_models()
            self.client.get(url_for('local_dev.login_admin'))

            role = RoleDepartment.query.get(1)
            # Create the models: location, role_location, responsible
            self.assertTrue(current_user.is_admin)
            data = {'username': 'fernandor',
                    'valid': 'amarillo',
                    'type': 'user'
                    }
            result = self.client.post(url_for('roles_bp.delegate', role_id=role.id), data=data,
                                      follow_redirects=True)
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Unable to set the delegate", str(result.data))
