from app.extensions import db
from app.phone_numbers.models import ResponsibleUser
from app.roles.mappers import RoleLocationMapper
from app.roles.models import RoleType, RoleLocation
from tests import BaseTestCase


class RolesModelsTest(BaseTestCase):

    # def test_visible_rooms_normal_user_outside_delegated(self):
    #     with self.client:
    #         # Login the user
    #         self.app.config["OFFICE_MODE"] = False
    #         self.client.get(url_for('local_dev.login_normal_user'))
    #         # Create the models
    #         responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
    #                                       last_name="Macklin", first_name="Burt")
    #         existing_delegate = DelegatedUserMapper.create_delegated_user(name="fernandor2",
    #                                                                       delegated_type=DelegatedType.USER,
    #                                                                       commit=True)
    #         db.session.add(responsible)
    #
    #         db.session.commit()
    #         #

    # def test_visible_rooms_normal_user_office_delegated(self):
    #     with self.client:
    #         # Login the user
    #         self.client.get(url_for('local_dev.login_normal_user'))
    #         # Create the models
    #
    #         responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
    #                                       last_name="Macklin", first_name="Burt")
    #         existing_delegate = DelegatedUserMapper.create_delegated_user(name="fernandor2",
    #                                                                       delegated_type=DelegatedType.USER,
    #                                                                       commit=True)
    #
    #         phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345",
    #                                                              display_name="Number 12345",
    #                                                              building='1',
    #                                                              floor='1',
    #                                                              room='1',
    #                                                              total_calls=25
    #                                                              )
    #         # Needs at least one phone to return the room as available
    #         phone_number.department = "IT"
    #         db.session.add(responsible)
    #         db.session.commit()
    #
    #         role_dep = RoleDepartmentMapper.create_role_department(department="IT", commit=True)
    #         role_dep.delegate = existing_delegate
    #         existing_delegate.dep_roles.append(role_dep)
    #         db.session.commit()
    #         #


    def test_role_location(self):
        responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
                                      last_name="Macklin", first_name="Burt")
        db.session.add(responsible)
        db.session.commit()

        role_location = RoleLocationMapper.create_role_location(role=RoleType.TSO, building='1',
                                                                floor='1',
                                                                room='1', commit=True)
        self.assertIsNotNone(role_location)
        roles = RoleLocation.query.all()
        self.assertEqual(len(roles), 1)
