from flask import url_for

from tests import BaseTestCase


class LocalDevTest(BaseTestCase):
    """
    SELECT NUMBER VIEW
    """

    def test_login_admin(self):
        with self.client:
            result = self.client.get(url_for('local_dev.login_admin'), follow_redirects=True)
            expected_content = 'Signed in as Burt Macklin Admin (admin)'
            self.assertIn(expected_content, str(result.data))
            self.assertEqual(result.status_code, 200)

    def test_login_normal_user(self):
        with self.client:
            result = self.client.get(url_for('local_dev.login_normal_user'), follow_redirects=True)
            expected_content = 'Signed in as Burt Macklin (fernandor2)'
            self.assertIn(expected_content, str(result.data))
            self.assertEqual(result.status_code, 200)
