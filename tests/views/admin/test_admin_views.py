from flask import url_for

from app.extensions import db
from app.phone_numbers.mappers import PhoneNumberMapper
from app.phone_numbers.models import ResponsibleUser
from app.phone_numbers.views.formatters import user_formatter
from app.roles.mappers import RoleLocationMapper, DelegatedUserMapper
from app.roles.models import RoleType, DelegatedType
from app.roles.views.formatters import delegate_formatter, role_formatter
from tests import BaseTestCase


class AdminViewsTest(BaseTestCase):
    def test_admin_home_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('admin.index'))

            self.assertEqual(result.status_code, 200)
            self.assertIn("Admin - Home", str(result.data))
            self.assertTemplateUsed('admin/home.html')

    def test_admin_home_view_not_logged_in(self):
        with self.client:
            result = self.client.get(url_for('admin.index'))

            self.assertEqual(result.status_code, 200)
            self.assertIn("Admin - Home", str(result.data))
            self.assertTemplateUsed('admin/home.html')

    def test_users_formatter(self):
        PhoneNumberMapper.create_phone_number(phone_number='1234', display_name='1234', given_name='12345',
                                              building='1',
                                              floor='1',
                                              room='1',
                                              commit=True)
        responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
                                      last_name="Macklin", first_name="Burt")
        db.session.add(responsible)
        db.session.commit()

        role_location = RoleLocationMapper.create_role_location(role=RoleType.TSO, building='1',
                                                                floor='1',
                                                                room='1', commit=True)
        role_location.responsibles.append(responsible)
        db.session.commit()

        result = user_formatter(None, None, role_location, None)
        self.assertIsNotNone(result)
        self.assertIn("?id=1", result)
        self.assertIn("user/edit", result)

    def test_delegate_formatter(self):
        PhoneNumberMapper.create_phone_number(phone_number='1234', display_name='1234', given_name='12345',
                                              building='1',
                                              floor='1',
                                              room='1',
                                              commit=True)
        responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
                                      last_name="Macklin", first_name="Burt")
        db.session.add(responsible)
        db.session.commit()

        role_location = RoleLocationMapper.create_role_location(role=RoleType.TSO, building='1',
                                                                floor='1',
                                                                room='1', commit=True)
        role_location.responsibles.append(responsible)
        responsible.roles.append(role_location)
        db.session.commit()

        existing_delegate = DelegatedUserMapper.create_delegated_user(name="not_admin",
                                                                      delegated_type=DelegatedType.USER,
                                                                      commit=True)

        role_location.delegate = existing_delegate
        existing_delegate.roles.append(role_location)
        db.session.commit()

        result = delegate_formatter(None, None, role_location, None)
        self.assertIsNotNone(result)
        self.assertIn("?id=1", result)
        self.assertIn("delegates/edit", result)

    def test_role_formatter(self):

        PhoneNumberMapper.create_phone_number(phone_number='1234', display_name='1234', given_name='12345',
                                                       building='1',
                                                       floor='1',
                                                       room='1',
                                                       commit=True)
        responsible = ResponsibleUser(username="fernanro", person_id="12345", email="blablabla@cern.csg",
                                    last_name="Macklin", first_name="Burt")
        db.session.add(responsible)
        db.session.commit()

        role_location = RoleLocationMapper.create_role_location(role=RoleType.TSO, building='1', commit=True)
        role_location.responsibles.append(responsible)
        responsible.roles.append(role_location)
        db.session.commit()

        result = role_formatter(None, None, responsible, None)
        self.assertIsNotNone(result)
        self.assertIn("?id=1", result)
        self.assertIn("role_location/edit", result)


