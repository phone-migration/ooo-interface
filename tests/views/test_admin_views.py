from flask import url_for

from tests import BaseTestCase


class AdminViewsTest(BaseTestCase):
    def test_admin_home_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('admin.index'))

            self.assertEqual(result.status_code, 200)
            self.assertIn("Admin - Home", str(result.data))
            self.assertTemplateUsed('admin/home.html')

    def test_admin_home_view_not_logged_in(self):
        with self.client:
            result = self.client.get(url_for('admin.index'))

            self.assertEqual(result.status_code, 200)
            self.assertIn("Admin - Home", str(result.data))
            self.assertTemplateUsed('admin/home.html')

    def test_admin_user_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('admin.user.index_view'))
            self.assertEqual(result.status_code, 200)
            self.assertIn("<h2 id=\"brand\">Admin - Responsibles</h2>", str(result.data))
            self.assertTemplateUsed('admin/model/list.html')

    def test_admin_user_view_not_logged_in(self):
        with self.client:
            result = self.client.get(url_for('admin.user.index_view'))
            self.assertEqual(result.status_code, 403)

    def test_admin_phone_migration_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('admin.numbers.index_view'))
            self.assertEqual(result.status_code, 200)
            self.assertIn("Admin - Numbers", str(result.data))
            self.assertTemplateUsed('admin/model/list.html')

    def test_admin_migration_view_not_logged_in(self):
        with self.client:
            result = self.client.get(url_for('admin.numbers.index_view'))
            self.assertEqual(result.status_code, 403)
