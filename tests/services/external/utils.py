import json

from httmock import urlmatch, response

import logging

logger = logging.getLogger(__name__)


@urlmatch(netloc=r'(.*\.)?ucservices\.web\.cern.ch(.*)', path="/api/phonenumbers/userHasAlreadyAPhoneNumber/")
def fim_request_mock(url, request):
    logger.debug("Calling has personal number mock")
    headers = {'content-type': 'text/html'}
    content = "false"
    return response(200, content, headers, None, 5, request)


@urlmatch(netloc=r'(.*\.)?ucservices\.web\.cern.ch(.*)', path="/api/phonenumbers/createPhoneNumber/")
def fim_create_phone_number_mock(url, request):
    logger.debug("Calling create phone number mock")
    headers = {'content-type': 'application/json'}
    content = [{"PhoneNumberValue": "46664", "Source": "phonebook", "OtherOwners": []},
               {"PhoneNumberValue": "46664", "Source": "foundation", "OtherOwners": []}]
    return response(200, json.dumps(content), headers, None, 5, request)