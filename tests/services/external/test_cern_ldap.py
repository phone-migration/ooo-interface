from app.services.cern_ldap import CernLdapClient
from tests import BaseTestCase


class CernLdapTest(BaseTestCase):

    def test_cern_ldap_not_found(self):
        ldap_client = CernLdapClient()
        with self.assertRaises(AssertionError):
            ldap_client.get_user_ldap("fernanro")
            self.assertTrue(False)

    def test_cern_ldap_get_user(self):
        ldap_client = CernLdapClient()

        result = ldap_client.get_user_ldap("fernandor")

        self.assertEqual(type(result), dict)
        self.assertEqual(result['cn'], "fernandor")

        self.assertEqual(result['mail'], "burt.macklin@cern.ch")
        self.assertEqual(result['displayName'], "Burt Macklin Test")
