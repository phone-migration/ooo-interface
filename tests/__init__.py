import os
from flask_testing import TestCase
from flask import current_app
from app.app_factory import create_app
from app.extensions import db
from app.phone_numbers.utils import create_initial_settings
from app.settings import FileConfiguration
from tests import config_test


def create_test_app():
    _basedir = os.path.abspath(os.path.dirname(__file__))

    config = FileConfiguration(config_test)
    application = create_app(config)
    application.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')

    return application


class BaseTestCase(TestCase):

    def create_app(self):
        return create_test_app()

    def setUp(self):
        current_app.logger.debug("Creating database")
        db.create_all()
        create_initial_settings()

    def tearDown(self):
        current_app.logger.debug("Removing database")
        db.session.remove()
        db.drop_all()
