import unittest

from app.settings import FileConfiguration
from app.app_factory import create_app
from tests import config_test


class AppFactoryTest(unittest.TestCase):

    def test_app_factory(self):
        config = FileConfiguration(config_test)
        app = create_app(config)

        self.assertTrue(app)
