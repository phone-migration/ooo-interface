from app.roles.mappers import RoleDepartmentMapper

from app.phone_numbers.models import ResponsibleUser
from flask_login import current_user

from app.phone_numbers.controllers import has_owner_personal_phone, user_can_migrate_number
from flask import url_for

from app.extensions import db
from app.phone_numbers.mappers import PhoneNumberMapper, PhoneOwnerMapper
from tests import BaseTestCase


class PhoneNumberControllersTest(BaseTestCase):

    def test_doesnt_have_owner_personal_phone(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))
            phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                                 display_name="Number 12345",
                                                                 given_name="",
                                                                 building='1',
                                                                 floor='1',
                                                                 room='1',
                                                                 total_calls=25, commit=True)

            db.session.commit()

            self.assertFalse(has_owner_personal_phone(current_user.username, phone_number.phone_number))

    def test_has_owner_personal_phone(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))
            phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                                 display_name="Number 12345",
                                                                 given_name="",
                                                                 building='1',
                                                                 floor='1',
                                                                 room='1',
                                                                 total_calls=25, commit=True)

            new_user = PhoneOwnerMapper.create_phone_owner(username=current_user.username,
                                                           name="Burt",
                                                           email=current_user.email,
                                                           commit=True)

            phone_number.is_personal = True
            phone_number.owner = new_user
            db.session.commit()

            phone_number2 = PhoneNumberMapper.create_phone_number(phone_number="09876",
                                                                  display_name="Number 09876",
                                                                  given_name="",
                                                                  building='1',
                                                                  floor='1',
                                                                  room='1',
                                                                  total_calls=25, commit=True)

            self.assertTrue(has_owner_personal_phone(current_user.username, phone_number2.phone_number))

    def test_user_cant_migrate_number(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))
            phone_number = PhoneNumberMapper.create_phone_number(phone_number="09876",
                                                                 display_name="Number 09876",
                                                                 given_name="",
                                                                 building='1',
                                                                 floor='1',
                                                                 room='1',
                                                                 total_calls=25, commit=True)

            responsible = ResponsibleUser(username=current_user.username, person_id="12345", email="blablabla@cern.csg",
                                          last_name="Macklin", first_name="Burt")
            db.session.add(responsible)
            db.session.commit()

            self.assertFalse(user_can_migrate_number(phone_number.phone_number))


    def test_user_can_migrate_number(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))
            phone_number = PhoneNumberMapper.create_phone_number(phone_number="09876",
                                                                 display_name="Number 09876",
                                                                 given_name="",
                                                                 building='1',
                                                                 floor='1',
                                                                 room='1',
                                                                 total_calls=25, commit=True)

            phone_number.department = "IT"

            responsible = ResponsibleUser(username=current_user.username, person_id="12345", email="blablabla@cern.csg",
                                          last_name="Macklin", first_name="Burt")
            responsible.department = "IT"
            db.session.add(responsible)
            db.session.commit()

            role_department = RoleDepartmentMapper.create_role_department(department="IT", commit=True)
            role_department.responsibles.append(responsible)

            db.session.commit()

            self.assertTrue(user_can_migrate_number(phone_number.phone_number))
