from app.phone_numbers.stats_controller import get_roles_department_stats, get_roles_location_stats
from flask import url_for
from tests import BaseTestCase


class ReviewViewsTest(BaseTestCase):

    def test_index_reachable(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('migrations.index'))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("total phones", str(result.data))

    def test_impersonate(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            data = {
                'username': 'fernandor2'
            }

            result = self.client.post(url_for('migrations.index'), data=data, follow_redirects=True)
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Logged in as", str(result.data))

    def test_impersonate_error(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            data = {
                'username': 'fernand'
            }

            result = self.client.post(url_for('migrations.index'), data=data, follow_redirects=True)
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Unable to find the selected user", str(result.data))

    def test_get_roles_department_stats(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            expected = {
                "completed": 0,
                "not_completed": 0,
                "not_processed": 0,
                "percent_completed": 0,
                "percent_processed": 0,
                "processed": 0,
                "total": 0
            }

            result = get_roles_department_stats()
            self.assertEqual(result, expected)

    def test_get_roles_location_stats(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            expected = {
                "completed": 0,
                "not_completed": 0,
                "not_processed": 0,
                "percent_completed": 0,
                "percent_processed": 0,
                "processed": 0,
                "total": 0
            }

            result = get_roles_location_stats()
            self.assertEqual(result, expected)
