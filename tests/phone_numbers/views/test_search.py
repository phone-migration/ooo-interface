import json

from flask import url_for

from tests import BaseTestCase


class SearchViewsTest(BaseTestCase):
    def test_search_users_egroups_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('migrations.search_users_egroups', q="Burt Macklin"))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Burt Macklin", str(result.data))
            results = json.loads(result.data.decode('utf-8'))["results"]
            self.assertEqual(len(results), 2)
            self.assertIn("Burt", results[0]["displayName"])

    def test_search_users_egroups_no_results_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('migrations.search_users_egroups', q="INVENT"))
            #
            self.assertEqual(result.status_code, 200)
            self.assertNotIn("Burt Macklin", str(result.data))
            results = json.loads(result.data.decode('utf-8'))["results"]
            self.assertEqual(len(results), 0)

    def test_search_users_no_results_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('migrations.search_users', q="INVENT"))
            #
            self.assertEqual(result.status_code, 200)
            self.assertNotIn("Burt Macklin", str(result.data))
            results = json.loads(result.data.decode('utf-8'))["results"]
            self.assertEqual(len(results), 0)

    def test_search_users_view(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))

            result = self.client.get(url_for('migrations.search_users', q="Burt Macklin"))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Burt Macklin", str(result.data))
            results = json.loads(result.data.decode('utf-8'))["results"]
            self.assertEqual(len(results), 2)
            self.assertIn("Burt", results[0]["displayName"])
