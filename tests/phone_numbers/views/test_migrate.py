import pytest
from flask import url_for
from flask_login import login_user
from httmock import HTTMock

from app.common.exceptions import AppException
from app.extensions import db
from app.phone_numbers.mappers import PhoneNumberMapper
from app.phone_numbers.models import PhoneAction, ResponsibleUser
from app.phone_numbers.migrate_controllers import create_new_owner, process_personal_number, process_keep_phone
from app.roles.mappers import RoleDepartmentMapper
from app.users.models import User
from tests import BaseTestCase
from tests.services.external.utils import fim_request_mock


@pytest.fixture(scope='module')
def create_phone():
    phone = PhoneNumberMapper.create_phone_number(phone_number="12345", display_name="Number 12345",
                                                  given_name="",
                                                  building='1',
                                                  floor='1',
                                                  room='1', total_calls=25, commit=True)

    phone.department = "IT"
    db.session.commit()
    return phone


@pytest.fixture(scope='module')
def create_responsible_user():
    responsible = ResponsibleUser(username="fernandor2", person_id=2, email="blablabla@cern.csg",
                                  last_name="Macklin", first_name="Burt")
    responsible.department = "IT"
    db.session.add(responsible)
    db.session.commit()

    role_department = RoleDepartmentMapper.create_role_department(department="IT", commit=True)

    if responsible not in role_department.responsibles:
        role_department.responsibles.append(responsible)

    db.session.commit()

    return responsible


class MigrateViewsTest(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.phone = create_phone()
        self.phone_number = self.phone.phone_number
        self.personal_action = PhoneAction(PhoneAction.APPLICATION)
        create_responsible_user()

        user = User("fernandor2",
                    person_id=2,
                    email="testnotadmin@cern.ch",
                    last_name="Macklin",
                    first_name="Burt",
                    is_admin=False)

        db.session.add(user)
        db.session.commit()

    def test_create_new_owner(self):
        with self.client:
            new_owner = create_new_owner("fernandor")
            self.assertIsNotNone(new_owner)

    def test_process_personal_number_ok(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.assertTrue(process_personal_number("fernandor", self.personal_action, self.phone_number))
                self.assertEqual(self.phone.action_to_take, self.personal_action)

    def test_process_personal_number_action_matches(self):
        with self.client:
            with HTTMock(fim_request_mock):
                action_to_take = PhoneAction(PhoneAction.APPLICATION)
                process_personal_number("fernandor", action_to_take, self.phone_number)
                self.assertEqual(self.phone.action_to_take, action_to_take)

    def test_process_keep_phone_not_logged(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.assertTrue(process_keep_phone(is_personal=True,
                                                   owner_username="fernandor",
                                                   personal_action=self.personal_action,
                                                   phone_number=self.phone_number,
                                                   trigger_username="fernandor2"))

    def test_process_keep_phone_ok(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.client.get(url_for('local_dev.login_normal_user'))
                print(self.phone_number)
                self.assertTrue(process_keep_phone(True, "fernandor", self.personal_action, self.phone_number))

    def test_process_keep_phone_owner_matches(self):
        with self.client:
            with HTTMock(fim_request_mock):
                process_keep_phone(True, "fernandor", self.personal_action, self.phone_number)
                self.assertEqual(self.phone.owner.username, "fernandor")

    def test_save_review_form_expect_405(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.client.get(url_for('local_dev.login_normal_user'))

                result = self.client.get(url_for('migrations.save_review_form'))

                self.assertEqual(result.status_code, 405)

    def test_save_review_form_expect_exception(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.client.get(url_for('local_dev.login_normal_user'))

                with self.assertRaises(AttributeError):
                    self.client.post(url_for('migrations.save_review_form'))

    def test_save_review_form_expect_result(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.client.get(url_for('local_dev.login_normal_user'))

                """
                phone_number = request.json.get("phone", None)
                is_present = request.json.get("isPresent", None)
                is_personal = request.json.get("isPersonal", None)
                want_to_keep = request.json.get("wantToKeep", None)
                action_to_take = request.json.get("actionToTake", None)
                owner = request.json.get("owner", None)
                """

                data = {
                    "phone": '12345',
                    "isPresent": True,
                    "isPersonal": False,
                    "wantToKeep": True,
                    "actionToTake": self.personal_action.value,
                    "owner": {"username": "fernandor"}
                }

                result = self.client.post(url_for('migrations.save_review_form'), json=data)

                self.assertIn("Phone 12345 reviewed successfully", str(result.data))


    def test_migrate_form_expect_result(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.client.get(url_for('local_dev.login_normal_user'))

                data = {
                    "phone": '12345'
                }

                result = self.client.post(url_for('migrations.migrate_form'), json=data)

                self.assertIn("Phone 12345 migrated successfully", str(result.data))


    def test_review_and_migrate_expect_result(self):
        with self.client:
            with HTTMock(fim_request_mock):
                self.client.get(url_for('local_dev.login_normal_user'))

                data = {
                    "phone": '12345',
                    "isPresent": True,
                    "isPersonal": True,
                    "wantToKeep": True,
                    "hasIpSocket": "-",
                    "actionToTake": self.personal_action.value,
                    "owner": {"username": "fernandor"}
                }

                result = self.client.post(url_for('migrations.save_review_form'), json=data)

                self.assertIn("Phone 12345 reviewed successfully", str(result.data))

                data = {
                    "phone": '12345'
                }

                result = self.client.post(url_for('migrations.migrate_form'), json=data)

                self.assertIn("Phone 12345 migrated successfully", str(result.data))
