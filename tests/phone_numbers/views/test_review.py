from flask_login import current_user

from app.extensions import db
from app.phone_numbers.mappers import PhoneNumberMapper
from app.phone_numbers.models import ResponsibleUser
from flask import url_for

from app.roles.mappers import RoleLocationMapper, RoleDepartmentMapper, DelegatedUserMapper
from app.roles.models import DelegatedType, RoleType
from tests import BaseTestCase

import logging

logger = logging.getLogger(__name__)


class ReviewViewsTest(BaseTestCase):

    def create_models(self):
        responsible = ResponsibleUser(username="fernandor2", person_id=2, email="blablabla@cern.csg",
                                      last_name="Macklin", first_name="Burt")
        existing_delegate = DelegatedUserMapper.create_delegated_user(name="fernandor2",
                                                                      delegated_type=DelegatedType.USER,
                                                                      commit=True)

        db.session.add(responsible)

        phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345", display_name="Number 12345",
                                                             given_name="",
                                                             building='1',
                                                             floor='1',
                                                             room='1', total_calls=25, commit=True)

        phone_number.department = "IT"
        db.session.commit()

        role_location = RoleLocationMapper.create_role_location(role=RoleType.TSO, building='1',
                                                                floor='1',
                                                                room='1', commit=True)
        role_location.delegate = existing_delegate
        role_location.responsibles.append(responsible)
        self.assertTrue(len(role_location.responsibles), 1)
        existing_delegate.roles.append(role_location)

        db.session.commit()

    def test_review_reachable(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))
            # Department
            role_department = RoleDepartmentMapper.create_role_department(department="EP", commit=True)
            # Phone
            phone = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                          display_name="Phone 12345",
                                                          room="1",
                                                          floor="1",
                                                          building="1",
                                                          total_calls="1",
                                                          commit=True)
            phone.department = "EP"
            db.session.commit()

            new_user = ResponsibleUser(username=current_user.username,
                                       person_id="12345",
                                       email="burt.macklin@cern.ch",
                                       last_name="Macklin",
                                       first_name="Burt"
                                       )
            db.session.add(new_user)
            role_department.responsibles.append(new_user)
            db.session.commit()

            result = self.client.get(url_for('migrations.review'), follow_redirects=True)
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("Review", str(result.data))
            self.assertIn("1", str(result.data))

    def test_review_non_existing_building(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))

            # Department
            role_department = RoleDepartmentMapper.create_role_department(department="EP", commit=True)
            # Phone
            phone = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                          display_name="Phone 12345",
                                                          room="1",
                                                          floor="1",
                                                          building="1",
                                                          total_calls="1",
                                                          commit=True)
            phone.department = "EP"
            db.session.commit()

            new_user = ResponsibleUser(username=current_user.username,
                                       person_id="12345",
                                       email="burt.macklin@cern.ch",
                                       last_name="Macklin",
                                       first_name="Burt"
                                       )
            db.session.add(new_user)
            role_department.responsibles.append(new_user)
            db.session.commit()

            result = self.client.get(url_for('migrations.review_building', building_name='5'))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("There is no phone number to review in this location.", str(result.data))

    def test_review_not_accesible_building(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))

            # Phone
            phone = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                          display_name="Phone 12345",
                                                          room="1",
                                                          floor="1",
                                                          building="1",
                                                          total_calls="1",
                                                          commit=True)
            phone.department = "EP"
            db.session.commit()

            result = self.client.get(url_for('migrations.review_building', building_name='1'))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("There is no phone number to review in this location.", str(result.data))

    def test_review_accesible_building(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))

            # Department
            role_department = RoleDepartmentMapper.create_role_department(department="EP", commit=True)
            # Phone
            phone = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                          display_name="Phone 12345",
                                                          room="1",
                                                          floor="1",
                                                          building="1",
                                                          total_calls="1",
                                                          commit=True)
            phone.department = "EP"
            db.session.commit()

            new_user = ResponsibleUser(username=current_user.username,
                                       person_id="12345",
                                       email="burt.macklin@cern.ch",
                                       last_name="Macklin",
                                       first_name="Burt"
                                       )
            new_user.department = "EP"
            db.session.add(new_user)
            role_department.responsibles.append(new_user)
            db.session.commit()

            result = self.client.get(url_for('migrations.review_building', building_name='1'))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("This building has 1 phones.", str(result.data))

    def test_review_accesible_room(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))

            # Department
            role_department = RoleDepartmentMapper.create_role_department(department="EP", commit=True)
            # Phone
            phone = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                          display_name="Phone 12345",
                                                          room="1",
                                                          floor="1",
                                                          building="1",
                                                          total_calls="1",
                                                          commit=True)
            phone.department = "EP"
            db.session.commit()

            new_user = ResponsibleUser(username=current_user.username,
                                       person_id="12345",
                                       email="burt.macklin@cern.ch",
                                       last_name="Macklin",
                                       first_name="Burt"
                                       )
            new_user.department = "EP"
            db.session.add(new_user)
            role_department.responsibles.append(new_user)
            db.session.commit()

            result = self.client.get(
                url_for('migrations.review_room', building_name='1', floor_name='1', room_name='1'))

            self.assertEqual(result.status_code, 200)
            self.assertIn("This building has 1 phones.", str(result.data))
            self.assertIn("Room 1-1-1", str(result.data))

    def test_review_not_accesible_room(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))

            # Phone
            phone = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                          display_name="Phone 12345",
                                                          room="1",
                                                          floor="1",
                                                          building="1",
                                                          total_calls="1",
                                                          commit=True)
            phone.department = "EP"
            db.session.commit()

            result = self.client.get(
                url_for('migrations.review_room', building_name='1', floor_name='1', room_name='1'))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn("There is no phone number to review in this location.", str(result.data))
