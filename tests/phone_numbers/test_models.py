from flask import url_for

from app.extensions import db
from app.phone_numbers.mappers import PhoneNumberMapper
from app.phone_numbers.models import PhoneNumber
from tests import BaseTestCase


class MigrationsModelsTest(BaseTestCase):

    def test_get_phones_normal_user(self):
        with self.client:
            self.client.get(url_for('local_dev.login_normal_user'))
            phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                                 display_name="Number 12345",
                                                                 given_name="",
                                                                 building='1',
                                                                 floor='1',
                                                                 room='1',
                                                                 total_calls=25, commit=True)

            db.session.commit()
            self.assertIsNotNone(phone_number)
            numbers = PhoneNumber.query.all()
            self.assertEqual(len(numbers), 1)

    def test_get_phones_admin(self):
        with self.client:
            self.client.get(url_for('local_dev.login_admin'))
            phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345",
                                                                 display_name="Number 12345", given_name="",
                                                                 building='1',
                                                                 floor='1',
                                                                 room='1',
                                                                 total_calls=25, commit=True)

            db.session.commit()
            self.assertIsNotNone(phone_number)
            numbers = PhoneNumber.query.all()
            self.assertEqual(len(numbers), 1)

    def test_phone_number(self):
        phone_number = PhoneNumberMapper.create_phone_number(phone_number="12345", display_name="Number 12345",
                                                             given_name="",
                                                             building='1',
                                                             floor='1',
                                                             room='1', total_calls=25, commit=True)
        self.assertIsNotNone(phone_number)
        numbers = PhoneNumber.query.all()
        self.assertEqual(len(numbers), 1)
