from app.app_settings.models import AppSettings
from app.phone_numbers.utils import create_initial_settings
from tests import BaseTestCase


class UtilsModelsTest(BaseTestCase):

    def test_create_initial_settings(self):
        create_initial_settings()
        settings = AppSettings.query.get(1)
        self.assertIsNotNone(settings)
