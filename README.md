# OOO Interface

## Status

### Master

[![pipeline status](https://gitlab.cern.ch/phone-migration/ooo-interface/badges/master/pipeline.svg)](https://gitlab.cern.ch/phone-migration/ooo-interface/commits/master) [![coverage report](https://gitlab.cern.ch/phone-migration/ooo-interface/badges/master/coverage.svg)](https://gitlab.cern.ch/phone-migration/ooo-interface/commits/master)

### Production

[![pipeline status](https://gitlab.cern.ch/phone-migration/ooo-interface/badges/production/pipeline.svg)](https://gitlab.cern.ch/phone-migration/ooo-interface/commits/production) [![coverage report](https://gitlab.cern.ch/phone-migration/ooo-interface/badges/production/coverage.svg)](https://gitlab.cern.ch/phone-migration/ooo-interface/commits/production)

## Local Development

## Generating a self-signed certificate

Run the following command on the `ssl` folder to add SSL certificates on development:

    openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365

Run the application

```bash
docker-compose up
```

Clear Docker containers

```bash
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Access the project using localhost `127.0.0.1:8080` or `<hostname>:8080`

## Usage

Import phone numbers

```bash
flask init_ofphones
```

Import responsibles

```bash
flask init_users
```

## Tests
```bash
pytest
```

## Test deployment with local Openshift instance

```bash
oc cluster up
```

```bash
oc login -u system:admin
```

Deploy the application (Check deployment section `oc new-app...`)

## Deployment

```bash
oc new-app openshift/templates/flask-postgresql-persistent.json -p SOURCE_REPOSITORY_URL=https://gitlab.cern.ch/phone-migration/ooo-interface.git -p SOURCE_REPOSITORY_REF=openshift_master
```