"""
WSGI config for project project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""


from app.app_factory import create_app
from app.settings import EnvConfiguration
from app.common.utils.logger import setup_webapp_logs
from app.common.utils.cli import initialize_cli

application = None

if not hasattr(EnvConfiguration, 'IS_LOCAL') or EnvConfiguration.IS_LOCAL:
    """
    To make development more comfortable, if we are on a local machine we get the
    configuration from a config.py file.
    This way we don't need to rebuild the Docker image every time we make a change
    """
    from secret import config
    from app.settings import FileConfiguration

    settings = FileConfiguration(config)
    application = create_app(settings)
    setup_webapp_logs(application, webapp_log_path=config.WEBAPP_LOGS)
    initialize_cli(application)

else:
    """
    If we are not on a local machine, then we get the config from the environment variables.
    By default, all the variables that start with ENV_PREFIX will be loaded automatically if
    ENV_LOAD_ALL is True
    """
    application = create_app(EnvConfiguration)
    setup_webapp_logs(webapp_log_path=EnvConfiguration.LOGS_FILE, log_level="DEV")
    initialize_cli(application)


if __name__ == "__main__":
    """
    In order to be able to use Oauth, we need to use SSL certificates
    """
    application.run(host='0.0.0.0', port=application.config['APP_PORT'], debug=application.config['DEBUG'],
                    ssl_context=('/opt/app-root/src/ssl/cert.pem', '/opt/app-root/src/ssl/key.pem'))
