import React from 'react';
import {IpPhoneReviewAdvice} from "./Messages";


class ParentModal extends React.Component {

}

function RemoveYourselfLink() {
    return (
        <a target={'_blank'}
           href={`http://phone-migration.web.cern.ch/phone-migration/cleanup/#should-i-remove-the-phone-myself`}>
            please remove the phone yourself
        </a>
    )
}

function DontKeepItMessage() {
    return (
        <span>If possible <RemoveYourselfLink/>. If you do so, then please change
         the review to ‘No, there is no phone’. Otherwise a technician will come at a later stage to remove the phone.
            The phone number will be deactivated from the database as soon as the physical phone is removed.</span>

    )
}


export class ReviewModal extends ParentModal {

    getAction = () => {

        const notPresentMessage = `
        The phone number will be removed from the phones database.
        `;

        const applicationMessage = `
        The phone number will be used on an application
        `;

        const ipPhoneMessage = `
        The phone number will be used on an IP Phone
        `;


        if (!this.props.isPresent) {
            return notPresentMessage;
        }

        if (this.props.isPresent && !this.props.wantToKeep) {
            return notPresentMessage;
        }

        if (this.props.isPresent && this.props.wantToKeep) {
            if (this.props.actionToTake === 'APPLICATION') {
                return applicationMessage;
            }
            if (this.props.actionToTake === 'IP_PHONE') {
                return ipPhoneMessage;
            }
        }
    };

    getAdvice = () => {

        const notPresentMessage = `
        Phone is not present, therefore it will be removed from the phones database overnight.
        `;

        const applicationMessage = `
        The owner will be contacted when the actual migration takes place with instructions on how to join the new service.
        `;

        if (!this.props.isPresent) {
            return notPresentMessage;
        }

        if (this.props.isPresent && !this.props.wantToKeep) {
            return <DontKeepItMessage/>;
        }

        if (this.props.isPresent && this.props.wantToKeep) {
            if (this.props.actionToTake === 'APPLICATION') {
                return applicationMessage;
            }
            if (this.props.actionToTake === 'IP_PHONE') {
                return <IpPhoneReviewAdvice hasIpSocket={this.props.hasIpSocket}/>;
            }
        }
    };

    getOwner = () => {

        if (!this.props.isPresent) {
            return `-`;
        }

        if (this.props.isPresent && !this.props.wantToKeep) {
            return `-`;
        }

        if (this.props.isPresent && this.props.wantToKeep) {
            return this.props.owner
        }
    };

    render() {
        return (<div className={`ui modal phone-modal-${this.props.phoneId}`}>
            <i className="close icon"/>
            <div className="header">
                Review of phone number {this.props.phoneNumber}
            </div>
            <div className="content">
                <div className="description">
                    <p>Here is a summary of your review:</p>
                    <ul>
                        <li><strong>Phone</strong>: {this.props.phoneNumber}</li>
                        <li><strong>Owner</strong>: {this.getOwner()}</li>
                        <li><strong>Location</strong>: {this.props.location}</li>
                        <li><strong>Action</strong>: {this.getAction()}</li>
                        <li><strong>Advice</strong>: {this.getAdvice()}</li>
                    </ul>
                </div>
            </div>
            <div className="actions">
                <div className="ui cancel primary button">Cancel</div>
                <button
                    onClick={this.props.handleSubmit}
                    className={`ui button modal-confirm-${this.props.phoneId}`}>
                    Save this choice
                </button>
            </div>
        </div>)
    }
}
