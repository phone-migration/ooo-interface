import React from 'react';
import {ReviewModal} from "./Modals";
import {HelpMessages} from "./Messages";
import {ApplyButton} from "./buttons";
import {
    ActionToTakeField,
    HasIpSocket,
    IsPersonalField,
    IsPresentField,
    PhoneOwnerField,
    WantToKeepField
} from "./fields";


const getDifferenceBetweenDates = (datepart, fromdate, todate) => {
    datepart = datepart.toLowerCase();
    const diff = todate - fromdate;
    const divideBy = {
        w: 604800000,
        d: 86400000,
        h: 3600000,
        n: 60000,
        s: 1000
    };

    return Math.floor(diff / divideBy[datepart]);
};

export class ReviewForm extends React.Component {
    constructor(props) {

        super(props);
        this.state = {value: ''};

        let owner;
        if (props.owner !== undefined && props.owner !== '') {
            owner = JSON.parse(props.owner);
        } else {
            owner = undefined
        }

        this.state = {
            isPresent: this.props.present,
            wantToKeep: this.props.keep,
            isPersonal: this.props.personal,
            hasIpSocket: (this.props.reviewed) ? this.props.hasIpSocket : '-',
            actionToTake: this.props.actionToTake,
            owner: owner,
            name: (owner !== undefined) ? owner.name : '',
            reviewed: this.props.reviewed,
            error: false,
            message: false,
            formIsValid: false,
            editing: !this.props.reviewed,
            migrated: this.props.migrated,
            sendingForm: false
        };

    }

    handleSubmit = async (event, redirect = true) => {
        event.preventDefault();

        const data = {
            isPresent: this.state.isPresent,
            wantToKeep: this.state.wantToKeep,
            isPersonal: this.state.isPersonal,
            hasIpSocket: this.state.hasIpSocket,
            actionToTake: this.state.actionToTake,
            owner: this.state.owner,
            phone: this.props.phoneNumber,
        };

        this.setState({error: false, errorMessage: ""});
        this.setState({message: false, messageText: ""});

        $(`.ui.modal.phone-modal-${this.props.phoneId}`).modal('hide');

        try {
            this.setState({sendingForm: true});
            const result = await fetch(this.props.action, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data),
                credentials: 'include'
            });
            const jsonData = await result.json();
            if (jsonData.error) {
                this.setState({sendingForm: false});
                this.setState({error: true, errorMessage: jsonData.error.result});
            } else {
                this.setState({
                    message: true,
                    messageText: jsonData.results.result,
                    reviewed: true,
                    editing: false
                });
                if (redirect) {
                    window.location.href = "";
                }
            }
            return jsonData
        } catch (err) {
            this.setState({sendingForm: false});
            this.setState({error: true, errorMessage: err});
        }
    };


    handleIsPresentChange = (event) => {
        this.setState({editing: true});
        console.log(this.state.isPresent);
        this.setState({isPresent: (event.target.value === 'true')}, () => {
            this.verifyFormIsValid();
        });
    };

    hasIpSocketChange = (event) => {
        this.setState({editing: true});
        const element = event.target;
        const selectedValue = element.options[element.selectedIndex].value;

        this.setState({hasIpSocket: selectedValue}, () => {
            this.verifyFormIsValid();
        });
    };

    handleWantToKeepChange = (event) => {
        this.setState({editing: true});

        this.setState({wantToKeep: (event.target.value === 'true')}, () => {
            this.verifyFormIsValid();
        });
    };

    handleIsPersonalChange = (event) => {
        this.setState({editing: true});

        this.setState({isPersonal: (event.target.value === 'true')}, () => {
            this.verifyFormIsValid();
        });
    };

    handleActionToTakeChange = (event) => {
        console.log(`Changing action to take`);
        this.setState({editing: true});
        this.setState({actionToTake: event.target.value}, () => {
            this.verifyFormIsValid();
        });
    };

    componentDidMount = () => {
        this.loadJqueryElements();
        this.verifyFormIsValid();
    };

    componentDidUpdate = () => {
        this.loadJqueryElements();
    };

    verifyFormIsValid() {
        let valid = false;
        if (!this.state.isPresent) {
            valid = true;
        } else {
            if (this.state.isPresent && !this.state.wantToKeep) {
                valid = true;
            } else if (this.state.isPresent && this.state.wantToKeep) {

                if ((this.state.actionToTake === 'APPLICATION' || this.state.actionToTake === 'IP_PHONE')
                    && this.state.name !== undefined
                    && this.state.name !== ''
                    && this.state.owner !== undefined) {
                    valid = true;
                }

                if (this.state.actionToTake === 'IP_PHONE' && this.state.hasIpSocket === '-') {
                    console.log(`hasIpSocket: ${this.state.hasIpSocket}`);
                    valid = false;
                }

            }
        }
        console.log(`form valid: ${valid}`);
        this.setState({formIsValid: valid})
    }

    loadJqueryElements = () => {

        $('select.dropdown').dropdown();

        $('.ui.search')
            .search({
                apiSettings: {
                    url: '/search/users/?q={query}'
                },
                searchFields: [
                    'displayName',
                    'description',
                    'username',
                    'type'
                ],
                fields: {
                    title: 'displayName'
                },
                minCharacters: 3,
                onSelect: (result, response) => {
                    this.setState({owner: result, name: result.displayName}, () => {
                        this.verifyFormIsValid();
                    });
                },
                onResults: (response) => {
                    this.setState({owner: undefined});
                }
            });


        const phoneModal = $(`.ui.modal.phone-modal-${this.props.phoneId}`);
        if (phoneModal.length) {
            phoneModal.modal('attach events', `.modal-trigger-${this.props.phoneId}`, 'show');
        }
    };

    handleChange = (event) => {
        this.setState({name: event.target.value}, () => {
            this.verifyFormIsValid();
        });
    };

    getDaysLeftUntilDeadline() {
        const deadlineDateParts = this.props.globalDeadline.split('-');
        const deadlineDate = new Date(deadlineDateParts[0], deadlineDateParts[1] - 1, deadlineDateParts[2]);
        const today = new Date();

        return getDifferenceBetweenDates('d', deadlineDate, today);
    }

    render() {

        const daysLeft = this.getDaysLeftUntilDeadline();

        const disabledClass = (this.state.migrated || daysLeft >= 0) ? 'disabled' : '';
        const {sendingForm} = this.state;
        const displayDimmer = sendingForm ? 'active' : '';


        return (
            <div className="ui basic segment">
                <div className={`ui ${displayDimmer} inverted dimmer`}>
                    <div className="ui text loader">Loading...</div>
                </div>
                <form className={'ui form'}>
                    {this.state.error && <div className="ui red message">{this.state.errorMessage}</div>}
                    {this.state.message && <div className="ui green message">{this.state.messageText}</div>}

                    <div className="grouped fields">
                        <div className="ui form">
                            <div className="grouped fields">
                                <IsPresentField disabledClass={disabledClass} value={this.state.isPresent}
                                                onChange={this.handleIsPresentChange}
                                                disabled={this.state.migrated}/>

                                {this.state.isPresent
                                && <WantToKeepField disabledClass={disabledClass} value={this.state.wantToKeep}
                                                    onChange={this.handleWantToKeepChange}
                                                    disabled={this.state.migrated}/>}

                                {this.state.isPresent
                                && this.state.wantToKeep
                                && <IsPersonalField disabledClass={disabledClass} value={this.state.isPersonal}
                                                    onChange={this.handleIsPersonalChange}
                                                    disabled={this.state.migrated}/>}

                                {this.state.isPresent
                                && this.state.wantToKeep
                                && this.state.isPersonal !== undefined
                                && <ActionToTakeField personal={this.state.isPersonal}
                                                      actionToTake={this.state.actionToTake}
                                                      onChange={this.handleActionToTakeChange}
                                                      disabled={this.state.migrated}/>}

                                {this.state.isPresent
                                && this.state.wantToKeep
                                && this.state.actionToTake === 'IP_PHONE'
                                && <HasIpSocket disabledClass={disabledClass} value={this.state.hasIpSocket}
                                                onChange={this.hasIpSocketChange}
                                                disabled={this.state.migrated}
                                                reviewed={this.props.reviewed}
                                />}

                                {this.state.isPresent
                                && this.state.wantToKeep
                                && <PhoneOwnerField value={this.state.name} onChange={this.handleChange}
                                                    disabled={this.state.migrated}/>
                                }
                            </div>
                            <div className="fields">
                                <ApplyButton formIsValid={this.state.formIsValid} editing={this.state.editing}
                                             reviewed={this.state.reviewed} phoneId={this.props.phoneId}/>
                            </div>

                            {(this.props.reviewed && !this.props.migrated && !(this.state.actionToTake === 'REMOVE' || this.state.actionToTake === 'DOESNT_EXIST'))
                                ? <div className={'ui info message'}>
                                    <i className={'ui exclamation icon'}/> This phone number has been reviewed and
                                    its owner will be contacted when the migration process starts.
                                </div>
                                : ''
                            }

                            {(this.props.reviewed && !this.props.migrated && (this.state.actionToTake === 'REMOVE' || this.state.actionToTake === 'DOESNT_EXIST'))
                                ? <div className={'ui info message'}>
                                    <i className={'ui exclamation icon'}/> This phone number has been reviewed and it
                                    will be deactivated after the physical phone is removed
                                </div>
                                : ''
                            }
                        </div>
                    </div>

                    {!this.props.migrated && <ReviewModal
                        phoneId={this.props.phoneId}
                        phoneNumber={this.props.phoneNumber}
                        location={this.props.location}
                        isPresent={this.state.isPresent}
                        wantToKeep={this.state.wantToKeep}
                        actionToTake={this.state.actionToTake}
                        hasIpSocket={this.state.hasIpSocket}
                        owner={this.state.name}
                        handleSubmit={this.handleSubmit}
                    />}

                    {this.state.isPresent
                    && this.state.wantToKeep
                    && this.state.editing
                    && <HelpMessages/>}

                </form>
            </div>

        );
    }
};