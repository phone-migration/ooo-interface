'use strict';

import React from "react";

function GenerateHelpLink() {
    return (
        <a href="https://phone-migration.web.cern.ch/content/installing-new-ip-phone"
           title="Phone migration instructions" target="_blank">see step-by-step instructions</a>
    )
}

function GenerateCampusNetworkServiceLink() {
    return (
        <a target="_blank" href="https://cern.service-now.com/service-portal/service-element.do?name=Campus-Network">Campus
            Network Service</a>
    )
}

export function IpPhoneReviewAdvice(props) {
    console.log(`hasIpSocket?`);
    console.log(props.hasIpSocket);
    if (props.hasIpSocket === false || props.hasIpSocket === 'false') {
        return (
            <span>Please open a ticket straight away with the <GenerateCampusNetworkServiceLink/> to request installation of an IP socket.</span>
        )
    }

    return (
        <span>You can safely save your choice, the number will not be migrated yet.
            The owner of the phone will be contacted when the actual migration begins, and provided 
            instructions on how to install a new IP phone.
        </span>
    )
}

export function IpPhoneMigrationAdvice() {
    return (
        <span>
            Please make sure everything is ready for this migration (<GenerateHelpLink/>)
            as the old analogue phone will stop working as soon as you confirm the action.
        </span>
    )
}


export class HelpMessages extends React.Component {
    render() {
        return (<div className="info-want-keep">
            <div>
                <p><span className="ui circular blue label">1</span> This option is free and can be used if: </p>
                <ul>
                    <li>A computer is available near the location of the phone <i>or</i></li>
                    <li>The number can be redirected to another number or to a list of numbers (fixed or mobile)</li>
                </ul>
            </div>

            <div>
                <p><span className="ui circular green label">2</span> This option will require that you: </p>
                <ul>
                    <li><a href="https://edh.cern.ch/edhcat/Browser?command=showPage&argument=270232"
                           title="CERN Store">Acquire a new IP phone</a> (Polycom VVX 201 or VVX 411)
                    </li>
                    <li>Ensure the presence of an available IP socket nearby</li>
                    <li>Ensure the presence of an available power socket nearby</li>
                </ul>
            </div>
        </div>)
    }
};

