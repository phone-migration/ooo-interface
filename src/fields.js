import React from 'react';

export function IsPersonalField(props) {
    return <div className={`field ${props.disabledClass}`}>
        <label>Is this a personal or shared phone?</label>
        <select className={`ui ${props.disabledClass} dropdown `}
                value={props.value}
                onChange={props.onChange}
                disabled={props.disabled}
        >
            <option value={"true"}>It is personal</option>
            <option value={"false"}>It is shared</option>
        </select>
    </div>;
}

export function IsPresentField(props) {
    return <div className={`field ${props.disabledClass}`}><label>Is this phone present in the selected
        location?</label>
        <select className={`ui ${props.disabledClass} dropdown `}
                value={props.value}
                onChange={props.onChange}
                disabled={props.disabled}
        >
            <option value={false}>No, there is no phone</option>
            <option value={true}>Yes, there is a phone</option>
        </select></div>;
}

export function HasIpSocket(props) {

    return <div className={`field ${props.disabledClass}`}>
        <label>Is there a nearby IP socket?</label>
        <select className={`ui ${props.disabledClass} dropdown `}
                value={props.value}
                onChange={props.onChange}
                disabled={props.disabled}
        >
            <option value={'-'}>Please, select an option</option>
            <option value={false}>No, there is no IP socket</option>
            <option value={true}>Yes, there is an IP socket</option>
        </select></div>;
}


export function WantToKeepField(props) {
    return <div className={`field ${props.disabledClass}`}>
        <label>Do you want to keep this phone?</label>
        <select className={`ui ${props.disabledClass} dropdown `}
                value={props.value}
                onChange={props.onChange}
                disabled={props.disabled}
        >
            <option value={"false"}>No, I don't want to keep it</option>
            <option value={"true"}>Yes, I want to keep it</option>
        </select>
    </div>;
}

export class ActionToTakeField extends React.Component {

    render = () => {

        const {onChange, disabled, actionToTake} = this.props;

        const style = {
            paddingLeft: 10
        };

        return <div className="field">
            <label>Where will it be used?</label>
            <div className="">
                <input
                    name={"actionTaken"}
                    id={"application"}
                    type="radio"
                    value="APPLICATION"
                    checked={(actionToTake === "APPLICATION")}
                    onChange={onChange}
                    disabled={disabled}/>
                <label style={style} htmlFor={"application"}>It will be used on an application
                    <span className="ui circular blue label">1</span>
                </label>
            </div>
            <div className="">
                <input
                    name={"actionTaken"}
                    id={"ip_phone"}
                    type="radio"
                    value="IP_PHONE"
                    checked={(actionToTake === "IP_PHONE")}
                    onChange={onChange}
                    disabled={disabled}/>
                <label style={style} htmlFor={"ip_phone"}>It will be used on a physical IP Phone
                    <span className="ui circular green label">2</span>
                </label>
            </div>
        </div>;
    }

}

export function PhoneOwnerField(props) {
    return <div className="field">
        <label>Select who will be the owner of the phone:</label>
        <div className="ui search">
            <div className="ui icon input">
                <input value={props.value}
                       name="prompt"
                       className="prompt"
                       placeholder="Enter a full name"
                       onChange={props.onChange}
                       disabled={props.disabled}
                />
            </div>
            <div className="results"/>
        </div>
    </div>;
}