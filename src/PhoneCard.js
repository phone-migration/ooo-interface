import React from 'react';
import {ReviewForm} from "./ReviewForm";

export class PhoneCard extends React.Component {
    render() {
        let {number} = this.props;

        return (
            <div className="ui fluid card">
                 <div className="content">
                 <span className="right floated like icon"
                      title="Total calls in the past 12 months">{number.totalCalls} <i
                    className="ui phone icon"/></span>
                    <a className="header">{number.phoneNumber}</a>
                    <div className="meta">{number.displayName}</div>
                    <div className="description">

                        <ReviewForm
                            action="/save/"
                            phoneNumber={number.phoneNumber}
                            location={number.location}
                            reviewed={number.reviewed}
                            migrated={number.migrated}
                            present={number.present}
                            purpose={number.purpose}
                            keep={number.keep}
                            personal={number.personal}
                            hasIpSocket={number.socket}
                            actionToTake={number.actionToTake}
                            phoneId={number.phoneId}
                            owner={number.owner}
                            globalDeadline={number.globalDeadline}
                        />
                    </div>
                </div>
            </div>
        )
    }
}