'use strict';

import React from "react";
import ReactDOM from "react-dom";

const {detect} = require('detect-browser');

import {PhoneCard} from "./PhoneCard";

const browser = detect();

const NotValidBrowserMessage = () => {
    let isValidBrowser = false;
    if (browser && browser.name && browser.name === 'chrome' || browser.name === 'firefox') {
        isValidBrowser = true;
    }
    if (!isValidBrowser) {
        return <div className={'ui warning message'}>
            <i className="ui exclamation icon"></i> Your browser is not currently supported. Please use
            either <strong>Firefox</strong> or <strong>Chrome</strong>.</div>;
    }
    return ''
};

class ReviewFormList extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {

        let listItems = this.props.numbers.map((number, index) =>
            <PhoneCard key={`card-${index}`} number={number}/>
        );


        return (
            <div>
                <NotValidBrowserMessage/>
                {listItems}
            </div>
        )
    }
}


ReactDOM.render(
    <ReviewFormList {...window.props}/>,
    window.react_mount
);

