import React from 'react';

export function ApplyButton(props) {
    return <div className=" field">
        <button type="button"
                style={
                    {
                        display: ((!props.formIsValid && props.editing)
                        || (props.reviewed && !props.editing) ? "none" : "initial")
                    }
                }
                className={`ui primary button modal-trigger-${props.phoneId}`}>
            Apply
        </button>
    </div>;
}
