#
# Application
#
DEBUG = True
IS_LOCAL = True
TESTING = False
USE_PROXY = True
APP_PORT = 8080
SECRET_KEY = "E\xf0\xd2G\xd5\x0bJ\xfd\x0b\xc7\xdc\x8c\xfb\xbd\xf7\xcd&C\xa3\xbc\xc8\xf7\xeb5"


OFFICE_MODE = True

#
# Authorization
#
CERN_OAUTH_ADMIN_EGROUPS = "it-cda-ic"

#
# Analytics
#
ANALYTICS_GOOGLE_ID = ""
ANALYTICS_PIWIK_ID = ""

#
# LOGS
#
LOG_LEVEL = "DEV"
WEBAPP_LOGS = "/opt/app-root/logs/src/webapp.log"
SEND_EMAIL = True
MAIL_HOSTNAME = ""
MAIL_FROM = ""
MAIL_TO = ""

#
# OAUTH
#
CERN_OAUTH_CLIENT_ID = ""
CERN_OAUTH_CLIENT_SECRET = ""


#
# Database
#
DB_NAME = "postgres"
DB_PASS = "postgres"
DB_PORT = 5432
DB_SERVICE_NAME = "postgres"
DB_USER = "postgres"
APP_DB_ENGINE = None
#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = "redis"
CACHE_REDIS_PORT = "6379"
CACHE_OAUTH_TIMEOUT = 300

#
# FIM SERVICE
#
FIM_API_USER = ""
FIM_API_PASSWORD = ""
FIM_API_ENDPOINT = ""