var path = require('path');
var webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

var config = {
    entry: {
        reviews: ["@babel/polyfill", '../src/reviews.js']
    },
    output: {
        path: path.join(__dirname, '../static/js'),
        filename: 'reviews.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env', "@babel/preset-react"],
                        plugins: [
                            "@babel/plugin-proposal-class-properties"
                        ]
                    }
                }
            }
        ]
    },
    plugins: [],
    // optimization: {
    //     minimizer: [
    //         // we specify a custom UglifyJsPlugin here to get source maps in production
    //         new UglifyJsPlugin({
    //             cache: true,
    //             parallel: true,
    //             uglifyOptions: {
    //                 compress: false,
    //                 ecma: 6,
    //                 mangle: true
    //             },
    //             sourceMap: true
    //         })
    //     ]
    // }
};

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        })
    )
}

module.exports = config;
