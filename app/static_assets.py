import os
from webassets import Bundle
from webassets.env import RegisterError
from flask import current_app as app

_basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))


def register_assets(assets):
    """
    Loads all the javascript and css/scss files
    
    :param assets:
    :return: The generated webassets bundle
    """

    """
    Global assets needed everywhere both admin and frontend
    """
    try:
        assets.register(
            'libs_js_all',
            Bundle(
                'js/libs/jquery-2.2.4.min.js',
                'js/libs/semantic.min.js',
                output='gen/js/packed_libs.js')
        )
    except RegisterError:
        app.logger.info("Asset libs_js_all already exists. Skipping...")

    """
        Global assets needed everywhere both admin and frontend
        """
    try:
        assets.register(
            'frontend_js_all',
            Bundle(
                'js/main.js',
                output='gen/js/frontend_packed.js')
        )
    except RegisterError:
        app.logger.info("Asset frontend_js_all already exists. Skipping...")

    """
    Needed on the buildings views
    """
    try:
        assets.register(
            'buildings_js_all',
            Bundle(
                'js/buildings.js',
                output='gen/js/buildings_packed.js')
        )
    except RegisterError:
        app.logger.info("Asset buildings_js_all already exists. Skipping...")

    """
    Delegates assets
    """
    try:
        assets.register(
            'delegate_js',
            Bundle(
                'js/delegate.js',
                output='gen/js/delegates_packed.js')
        )
    except RegisterError:
        app.logger.info("Asset delegates_js already exists. Skipping...")

    try:
        assets.register(
            'index_js',
            Bundle(
                'js/index.js',
                output='gen/js/index_packed.js')
        )
    except RegisterError:
        app.logger.info("Asset index_js already exists. Skipping...")

    try:
        assets.register(
            'react_js',
            Bundle(
                'js/libs/babel.min.js',
                'js/libs/react.production.min.js',
                'js/libs/react-dom.production.min.js',
                output='gen/js/react_packed.js')
        )
    except RegisterError:
        app.logger.info("Asset reviews_js already exists. Skipping...")

    """
    All the Semantic UI css assets
    """
    try:
        assets.register(
            'libs_css_all',
            Bundle(
                'css/libs/*.css',
                output='gen/css/libs_packed.css')
        )
    except RegisterError:
        app.logger.info("Asset semantic_all already exists. Skipping...")

    """
    All the user frontend custom scss files. These files are processed with pyscss to convert them to css.
    """
    try:
        assets.register(
            'frontend_css_all',
            Bundle(
                'css/frontend/main.scss',
                'css/frontend/cern_toolbar.scss',
                'css/media-queries.scss',
                filters='pyscss',
                output='gen/css/frontend_packed.css')
        )
    except RegisterError:
        app.logger.info("Asset users_css_all already exists. Skipping...")
