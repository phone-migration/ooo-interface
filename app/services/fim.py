import logging

import random
import requests
from flask import current_app
from requests.auth import HTTPBasicAuth

logger = logging.getLogger(__name__)


class UCServicesException(Exception):
    pass


def user_has_personal_number_in_fim(username):
    """
    Makes a request against the FIM API to check if the user has already a phone number.
    :param username:
    :return:
    """
    logger.info("Checking personal number in FIM -> Checking user: {}".format(username))
    response = requests.get("{}/userHasAlreadyAPhoneNumber/{}".format(current_app.config["FIM_API_ENDPOINT"], username),
                            auth=HTTPBasicAuth(current_app.config["FIM_API_USER"],
                                               current_app.config["FIM_API_PASSWORD"]))
    result = response.text
    if result == "true":
        logger.info("User {} has a personal number".format(username))
        return True
    elif result == "false":
        logger.info("User {} doesn't have a personal number".format(username))
        return False
    else:
        raise UCServicesException("Unable to check user {}'s phone numbers.".format(username))


def create_phone_number_in_fim(username, phone_number, phone_type):
    """
    Creates a phone number against the FIM API and associates it to the user.
    :param username: String username
    :param phone_number: String of the phone number
    :param phone_type: (CommonArea | Personal)
    :return:
    """
    logger.info("Creating a new phone number in FIM is DISABLED")
    # if current_app.config["TESTING"] or current_app.config["DEBUG"]:
    #     # On testing mode we don't want to create phone numbers
    #     logger.info("TESTING MODE ENABLED -> Won't do phone creation. Returning True by default.")
    #     return True
    # else:
    #     # return True
    #     if current_app.config["DEBUG"]:
    #         # On Debug mode we will create random fake numbers as we don't want to affect the real ones
    #         logger.info("DEBUG MODE ENABLED -> Will use a fake phone number")
    #         username = "rcandido"
    #         # phone_number = "2221" + str(random.randint(0, 9))
    #         phone_number = "22213"
    #
    #     data = {
    #         'Number': phone_number,
    #         'SamAccountName': username,
    #         'PhoneNumberType': phone_type
    #     }
    #     logger.info("Making a request to the createPhoneNumber endpint -> User: {} Phone: {} Type: {}".format(username,
    #                                                                                                           phone_number,
    #                                                                                                           phone_type))
    #     response = requests.post("{}/createPhoneNumber/".format(current_app.config["FIM_API_ENDPOINT"], username),
    #                              auth=HTTPBasicAuth(current_app.config["FIM_API_USER"],
    #                                                 current_app.config["FIM_API_PASSWORD"]),
    #                              data=data)
    #     result = response.text
    #     if result == "0" and response.status_code == 200:
    #         logger.info(
    #             "FIM API returned a 0 -> Successful response for phone: {} user: {}".format(phone_number, username))
    #         return True
    #     elif response.status_code == 500:
    #         logger.warning(
    #             "FIM API returned 500 -> Unsuccessful response for phone: {} user: {}".format(phone_number, username))
    #         return False
    #     else:
    #         logger.warning(
    #             "FIM API returned 500 -> Unable to create phone number {} for user: {}".format(phone_number, username))
    #         raise UCServicesException("Unable to create phone number {} for user {}.".format(phone_number, username))
