import json

from flask import current_app
from ldap3 import Server, Connection, MOCK_SYNC
import logging

from app.extensions import cache

logger = logging.getLogger(__name__)


def generate_description(division, group, section):
    if division and group and section:
        return "{}-{}-{}".format(division, group, section)

    if division and group:
        return "{}-{}".format(division, group)

    if division:
        return "{}".format(division)


def user_to_json(ldap_data):
    """
    Converts the user data returned by LDAP to a dict
    :param ldap_data: Data returned by LDAP in dict format
    :return: A dict with the new user data
    """
    division = ldap_data["division"]
    group = ldap_data["cernGroup"]
    section = ldap_data["cernSection"]
    # logger.debug("Converting to JSON")
    return {'displayName': ldap_data["displayName"],
            'username': ldap_data["cn"],
            'personId': ldap_data["employeeID"],
            'email': ldap_data["mail"],
            'description': generate_description(division, group, section),
            'type': 'user'}


def egroup_to_json(ldap_data):
    """
    Converts the egroup data returned by LDAP to a dict
    :param ldap_data: Data returned by LDAP in dict format
    :return: A dict with the new egroup data
    """
    return {'displayName': str(ldap_data["cn"]),
            'username': str(ldap_data["cn"]),
            'description': "Egroup",
            'type': 'egroup'}


class CernLdapClient(object):
    """
    Class to make LDAP calls
    """
    server_url = None
    use_test_server = None
    server = None
    base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
    base_egroup_dn = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
    conn = None

    user_retrieve_attributes_full = ['cn',
                                     'sn',
                                     'cernGroup',
                                     'division',
                                     'cernSection',
                                     'uid',
                                     'physicalDeliveryOfficeName',
                                     'mail',
                                     'displayName',
                                     'telephoneNumber',
                                     'otherTelephone',
                                     'employeeID',
                                     'cernAccountType',
                                     'sn',
                                     'givenName',
                                     'physicalDeliveryOfficeName']

    user_egroups_attributes = ['memberOf']

    egroup_filter = ['cn']
    egroup_attributes = ['cn']

    def __init__(self, server_url="xldap.cern.ch"):
        """
        Initializes either a production or test server depending on the TESTING config variable
        :param server_url: Hostname of the LDAP server
        """
        if current_app.config['TESTING']:
            self.__configure_test_server()
        else:
            self.server_url = server_url
            self.server = Server(self.server_url)

    def __configure_test_server(self):
        """
        Initializes a test server with sample json responses
        :return:
        """
        self.server_url = 'fake_server'
        server_info = 'app/services/cern_ldap_sample_queries/my_real_server_info.json'
        server_schema = 'app/services/cern_ldap_sample_queries/my_real_server_schema.json'

        current_app.logger.debug("Loading test LDAP server")

        # Create a fake server from the info and schema json files
        self.server = Server.from_definition('my_fake_server', server_info, server_schema)

        conn = Connection(self.server, client_strategy=MOCK_SYNC)
        # Populate the DIT of the fake server
        conn.strategy.entries_from_json(
            'app/services/cern_ldap_sample_queries/my_real_server_entries.json')
        self.__set_connection(conn)
        self.conn.bind()

    def __set_connection(self, connection):
        self.conn = connection

    def __make_connection(self):
        """
        Initializes a new connection if none is created yet.
        :return: -
        """
        if not self.conn:
            self.conn = Connection(self.server, auto_bind=True)

    @cache.memoize(timeout=50)
    def get_user_ldap(self, username):
        """
        Gets a user from LDAP by username
        :param username: username that will be searched on LDAP
        :return: The first one of the entries if there is only one or None otherwise
        """
        self.__make_connection()
        search_filter = "(CN={})".format(username)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_retrieve_attributes_full)
        results = self.create_user_list_from_entries()
        self.conn.unbind()

        if len(results) != 1:
            return None

        return results[0]

    def get_user_ldap_by_person_id(self, person_id):
        """
        Gets a user from LDAP by person_id
        :param person_id: Unique person id that will be searched on LDAP
        :return: The first one of the entries if there is only one or None otherwise
        """
        self.__make_connection()
        search_filter = "(&(employeeID={})(cernAccountType=Primary))".format(person_id)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_retrieve_attributes_full)
        results = self.create_user_list_from_entries()
        self.conn.unbind()

        return results[0] if len(results) == 1 else None

    def get_egroup_by_name(self, name):
        self.__make_connection()
        search_filter = "(CN={})".format(name)
        self.conn.search(search_base=self.base_egroup_dn, search_filter=search_filter,
                         attributes=self.egroup_attributes)

        entry = self.conn.entries[0]
        self.conn.unbind()

        return entry

    @cache.memoize(timeout=50)
    def get_users_ldap(self, display_name):
        """
        Gets a list of users that matches part of the display name on the arguments
        :param display_name: Display name that will be searched
        :return: A list of users in LDAP format
        """
        logger.debug("Using normal function")
        self.__make_connection()
        search_filter = "(&(displayName={}*)(cernAccountType=Primary))".format(display_name)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_retrieve_attributes_full)
        results = self.create_user_list_from_entries()

        self.conn.unbind()

        return results

    def create_user_list_from_entries(self):
        results = []
        for entry in self.conn.entries:
            new = json.loads(entry.entry_to_json())['attributes']
            new_formatted = {}
            for key, val in new.items():
                try:
                    new_formatted[key] = val[0]
                except IndexError:
                    new_formatted[key] = None
            results.append(new_formatted)
        return results

    @cache.memoize(timeout=50)
    def get_egroups_by_name(self, name):
        """
        Gets a list of egroups matching by part of the name
        :param name: The name of the egroup that will be searched
        :return: A list of egroups in LDAP format
        """
        self.__make_connection()
        search_filter = "(CN={}*)".format(name)
        logger.debug(search_filter)
        self.conn.search(search_base=self.base_egroup_dn, search_filter=search_filter,
                         attributes=self.egroup_attributes)
        egroups_list = self.create_egroups_list_for_search(self.conn.entries)

        self.conn.unbind()
        return egroups_list

    @cache.memoize(timeout=50)
    def get_user_egroups(self, person_id):
        """
        Gets all the egroups of a user matching the person_id in the arguments
        :param person_id: Unique person id of the user
        :return: A list of egroups in LDAP format
        """
        self.__make_connection()
        # logger.debug("getting egroups for person_id: {}".format(person_id))

        search_filter = "(&(employeeID={})(cernAccountType=Primary))".format(person_id)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_egroups_attributes)

        egroups_list = self.create_egroups_list_from_entries(self.conn.entries)
        self.conn.unbind()

        return egroups_list

    def create_egroups_list_from_entries(self, result):
        egroups_list = []
        try:
            for egroups in result[0]:
                for egroup in egroups:
                    egroups_list.append(egroup.split(",")[0].split("=")[1])
        except IndexError:
            return []

        return egroups_list


    def create_egroups_list_for_search(self, result):
        results = []
        for entry in result:
            new = json.loads(entry.entry_to_json())['attributes']
            new_formatted = {}
            for key, val in new.items():
                try:
                    new_formatted[key] = val[0]
                except IndexError:
                    new_formatted[key] = None
            results.append(new_formatted)
        logger.debug(results)
        return results

    @cache.memoize(timeout=50)
    def get_user_egroups_by_username(self, username):
        """
        Gets all the egroups of a user matching the username in the arguments
        :param username: Username of the user
        :return: A list of egroups in LDAP format
        """
        self.__make_connection()
        search_filter = "(&(CN={})(cernAccountType=Primary))".format(username)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_egroups_attributes)

        egroups_list = self.create_egroups_list_from_entries(self.conn.entries)

        self.conn.unbind()

        return egroups_list
