import datetime
import logging

from flask import flash
from flask_login import current_user
from sqlalchemy.orm.exc import NoResultFound

from app.phone_numbers.controllers import has_owner_personal_phone
from app.phone_numbers.mappers import PhoneOwnerMapper
from app.phone_numbers.models import PhoneNumber, PhoneType, RequestStatus, PhoneAction, PhoneOwner
from app.services.cern_ldap import CernLdapClient, user_to_json
from app.services.fim import user_has_personal_number_in_fim, UCServicesException, \
    create_phone_number_in_fim

logger = logging.getLogger(__name__)

def create_new_owner(owner_username):
    logger.info("Creating a new phone owner: {}".format(owner_username))
    ldap_client = CernLdapClient()
    data = ldap_client.get_user_ldap(owner_username)
    ldap_owner = user_to_json(data)
    owner = PhoneOwnerMapper.create_phone_owner(
        name=ldap_owner['displayName'],
        organizational_unit=ldap_owner['description'],
        email=ldap_owner['email'].strip().lower(),
        username=ldap_owner['username'].strip(),
        commit=True)
    return owner


def process_personal_number(owner_username=None, personal_action=None, phone_number=None, trigger_username=None):
    """
    Processes the action of adding a personal number to the selected user
    :param trigger_username:
    :param owner_username: User's username
    :param personal_action: Selected action for the phone.
    :param phone_number: (String) Phone number
    :return: (True | False) if the creation was successful
    """
    logger.info("Processing phone {} as personal number".format(phone_number))
    phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
    phone.type = PhoneType.PERSONAL
    try:
        has_fim_number = user_has_personal_number_in_fim(owner_username)
        if has_fim_number:
            logger.warning("User {} has already a personal number in FIM.".format(owner_username))
            return False

        if has_owner_personal_phone(owner_username, phone_number):
            logger.warning("User {} has already a personal number.".format(owner_username))
            return False
    except UCServicesException as error:
        logger.error(str(error))
        flash(str(error), "error")

    phone.action_to_take = personal_action
    return True


def create_personal_number(owner_username, phone_number, trigger_username):
    """
    Tries to create a personal number for the user
    :param phone_number: String. Phone number to be created.
    :param trigger_username:
    :param owner_username: String. Username of the phone owner
    :return:
    """
    logger.info("Creating a personal number {} for user {}".format(phone_number, owner_username))
    if create_phone_number_in_fim(owner_username, phone_number, "Personal"):
        # Updating the phone number information in the database

        phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
        logger.warning(
            "SUCCESS: Created PERSONAL phone number on FIM. number: {}. username: {}. Triggered by: {}".format(
                phone.phone_number,
                owner_username,
                trigger_username))
        phone.completed = True
        phone.request_completed_date = datetime.datetime.now()
        phone.request_status = RequestStatus.COMPLETE
        return True
    else:
        # If it fails, we won't set the phone as completed on the database
        phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
        logger.warning(
            "WARNING: Unable to create PERSONAL phone number on FIM. number: {}. username: {}. Triggered by: {}".format(
                phone.phone_number,
                owner_username,
                trigger_username))
        phone.completed = False
        phone.request_completed_date = None
        return False


def process_keep_phone(is_personal=None, owner_username=None, personal_action=None, phone_number=None,
                       trigger_username=None):
    """
    Action taken when the user wants to keep a personal phone
    :param trigger_username:
    :param is_personal: (True | False) If the phone number is going to be personal or not
    :param owner_username:  String. Username of the user
    :param personal_action:
    :param phone_number: (String)
    :return:
    """
    phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
    phone.request_status = RequestStatus.PENDING
    logger.info("Processing phone {} to be kept".format(phone_number))
    if is_personal:
        personal_valid = process_personal_number(owner_username=owner_username,
                                                 personal_action=personal_action,
                                                 phone_number=phone.phone_number,
                                                 trigger_username=trigger_username)
        if not personal_valid:
            logger.debug("Is not a valid personal number")
            return False
    else:
        phone.type = PhoneType.SHARED
        phone.action_to_take = PhoneAction.IP_PHONE
    try:
        owner = PhoneOwner.query.filter_by(username=owner_username).one()
    except NoResultFound:
        owner = create_new_owner(owner_username)
    phone.owner = owner
    owner.phones.append(phone)
    return True


def create_common_area_number(owner_username, phone):
    """
    Tries to create a CommonArea number for a user
    :param owner_username: Username of the user
    :param phone: Phone number of the user
    :return:
    """

    if create_phone_number_in_fim(owner_username, phone.phone_number, "CommonArea"):
        # Updating the phone number information in the database
        logger.info(
            "SUCCESS: Created COMMON AREA phone number on FIM. number: {}. username: {}. Triggered by: {}".format(
                phone.phone_number,
                owner_username,
                current_user.username))
        phone.completed = True
        phone.request_completed_date = datetime.datetime.now()
        phone.request_status = RequestStatus.COMPLETE
        return True
    else:
        # If it fails, we won't set the phone as completed on the database
        logger.warning(
            "WARNING: Unable to create COMMON AREA phone number on FIM. number: {}. username: {}. Triggered by: {}".format(
                phone.phone_number,
                owner_username,
                current_user.username))
        phone.completed = False
        phone.request_completed_date = None
        return False