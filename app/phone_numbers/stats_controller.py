import logging

from app.phone_numbers.models import PhoneNumber
from app.roles.controllers import (get_all_user_locations,
                                   get_user_phones_from_locations,
                                   get_user_phones_from_departments)

logger = logging.getLogger(__name__)


def get_total_migration_stats():
    """
    Get all the stats for all the phone numbers in the database
    :return: a Json with the percent, processed and not processed phones and the total
    """
    phones_processed = PhoneNumber.query.filter_by(processed=True).all()
    phones_not_processed = PhoneNumber.query.filter_by(processed=False).all()
    total_phones = len(phones_processed) + len(phones_not_processed)

    try:
        percent_processed = int((len(phones_processed) / total_phones) * 100)
    except ArithmeticError:
        percent_processed = 0

    phones_completed = PhoneNumber.query.filter_by(completed=True).all()
    phones_not_completed = total_phones - len(phones_completed)
    try:
        percent_completed = int((len(phones_completed) / total_phones) * 100)
    except ArithmeticError:
        percent_completed = 0

    return {
        "percent_processed": percent_processed,
        "percent_completed": percent_completed,
        "not_processed": len(phones_not_processed),
        "processed": len(phones_processed),
        "completed": len(phones_completed),
        "not_completed": phones_not_completed,
        "total": total_phones,
    }


def get_roles_department_stats():
    """
    Gets all the statistics for the phones that have the same department that the RoleDepartment parameters

    :return: A dict with all the stats
    """
    phones = get_user_phones_from_departments()

    processed = 0
    completed = 0
    for phone in phones:
        if phone.processed:
            processed += 1
        if phone.completed:
            completed += 1

    total = len(phones)
    percent_processed = 0 if processed == 0 else int((processed / total) * 100)
    percent_completed = 0 if completed == 0 else int((completed / total) * 100)

    return {
        "percent_processed": percent_processed,
        "not_processed": total - processed,
        "processed": processed,
        "completed": completed,
        "percent_completed": percent_completed,
        "not_completed": total - completed,
        "total": total
    }


def get_roles_location_stats():
    """
    Gets all the stats for the RoleLocation objects in the arguments
    :return: A dict with the stats
    """

    buildings, floors, rooms = get_all_user_locations()
    phones = get_user_phones_from_locations(buildings, floors, rooms)

    processed = 0
    completed = 0
    for phone in phones:
        if phone.processed:
            processed += 1
        if phone.completed:
            completed += 1

    total = len(phones)
    percent_processed = 0 if processed == 0 else int((processed / total) * 100)
    percent_completed = 0 if completed == 0 else int((completed / total) * 100)

    return {
        "percent_processed": percent_processed,
        "not_processed": total - processed,
        "processed": processed,
        "completed": completed,
        "percent_completed": percent_completed,
        "not_completed": total - completed,
        "total": total
    }
