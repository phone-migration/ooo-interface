from app.extensions import db
from app.app_settings.models import AppSettings


def create_initial_settings():
    """
    Creates the initial settings for the application if they don't exist yet.
    :return: -
    """
    settings = AppSettings.query.get(1)
    if not settings:
        settings = AppSettings()
        db.session.add(settings)
        db.session.commit()
