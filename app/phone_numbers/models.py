import enum
import json

from app.common.models import ModelBase
from app.locations.constants import purposes
from app.users.models import BaseUser, User

from app.extensions import db

import logging

logger = logging.getLogger(__name__)


class ResponsibleUser(BaseUser):
    __tablename__ = 'responsible_user'


class PhoneAction(enum.Enum):
    DOESNT_EXIST = "DOESNT_EXIST"
    REMOVE = "REMOVE"
    APPLICATION = "APPLICATION"
    IP_PHONE = "IP_PHONE"

    @classmethod
    def choices(cls):
        result = [(str(choice.name), choice) for choice in cls]
        return result

    def __str__(self):
        return str(self.value)


class PhoneType(enum.Enum):
    PERSONAL = "Personal"
    SHARED = "Shared"

    @classmethod
    def choices(cls):
        result = [(str(choice.name), choice) for choice in cls]
        return result

    def __str__(self):
        return str(self.value)


class PhoneOwner(ModelBase):
    __tablename__ = 'phone_owner'

    name = db.Column(db.String(255), nullable=False)
    organization_unit = db.Column(db.String(255))
    username = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)

    def __str__(self):
        return "{}".format(self.name)

    def to_json(self):
        return json.dumps({'name': self.name, 'username': self.username})


class RequestStatus(enum.Enum):
    PENDING = "Action is pending"
    PENDING_DELETE = "Deletion is pending"
    PENDING_MIGRATE = "Migration is pending"
    COMPLETE = "Action is completed"
    ERROR = "Error"

    @classmethod
    def choices(cls):
        result = [(str(choice.name), choice) for choice in cls]
        return result

    def __str__(self):
        return str(self.value)


class PhoneNumber(ModelBase):
    """
    Represents an application User
    """
    __tablename__ = 'phone_number'

    phone_number = db.Column(db.String(255), nullable=False, unique=True)
    given_name = db.Column(db.String(255))
    display_name = db.Column(db.String(255))
    processed = db.Column(db.Boolean, default=False)
    completed = db.Column(db.Boolean, default=False)
    total_calls = db.Column(db.Integer, default=0)
    purpose = db.Column(db.String(255))

    department = db.Column(db.String(255))
    org_unit = db.Column(db.String(255))

    building = db.Column(db.String(255))
    floor = db.Column(db.String(255))
    room = db.Column(db.String(255))

    """
    Actions
    """
    owner_id = db.Column(db.Integer, db.ForeignKey('phone_owner.id'))
    owner = db.relationship(PhoneOwner, foreign_keys=owner_id, backref='phones')

    action_to_take = db.Column(db.Enum(PhoneAction))
    type = db.Column(db.Enum(PhoneType))
    is_present = db.Column(db.Boolean, default=False)
    want_to_keep = db.Column(db.Boolean, default=False)
    is_personal = db.Column(db.Boolean, default=True)
    has_ip_socket = db.Column(db.Boolean, default=False)

    request_date = db.Column(db.DateTime)
    request_status = db.Column(db.Enum(RequestStatus))
    request_completed_date = db.Column(db.DateTime)
    request_processed_date = db.Column(db.DateTime)
    request_user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    request_user = db.relationship(User, foreign_keys=request_user_id)

    def __str__(self):
        return self.phone_number

    @property
    def purpose_description(self):
        return purposes[self.purpose]

    @property
    def location(self):
        return "{}-{}-{}".format(self.building, self.floor, self.room)
