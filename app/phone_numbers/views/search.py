from flask import request, jsonify

from app.phone_numbers.blueprint import migrations_blueprint
from app.services.cern_ldap import CernLdapClient, user_to_json, egroup_to_json
from app.common.views.decorators import requires_login
from app.phone_numbers.controllers import has_owner_personal_phone

import logging

logger = logging.getLogger(__name__)


@migrations_blueprint.route('/search/users-egroups/', methods=["GET"])
@requires_login
def search_users_egroups():
    """
    Performed to retrieve delegates: users and egroups
    :return: Response with json content
    """
    query = request.args.get('q')
    ldap_client = CernLdapClient()
    results = ldap_client.get_users_ldap(query)
    ldap_client = CernLdapClient()
    results_egroups = ldap_client.get_egroups_by_name(query)
    users = [user_to_json(user) for user in results]
    egroups = [egroup_to_json(egroup) for egroup in results_egroups]

    return jsonify(results=users + egroups)


@migrations_blueprint.route('/search/users/', methods=["GET"])
@requires_login
def search_users():
    """
    Performed to retrieve only users. Used for the impersonation.
    :return: Response with json content
    """
    query = request.args.get('q')
    ldap_client = CernLdapClient()
    results = ldap_client.get_users_ldap(query)
    users = [user_to_json(user) for user in results]

    return jsonify(results=users)


@migrations_blueprint.route('/search/personal/', methods=["GET"])
@requires_login
def has_personal_phone():
    """
    Checks if the selected user has already a personal phone
    :return: Response with json content
    """
    query = request.args.get('q')
    if has_owner_personal_phone(query):
        return jsonify(results=True)
    return jsonify(results=False)
