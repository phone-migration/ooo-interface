import datetime
import logging

from app.phone_numbers.migrate_controllers import process_keep_phone
from flask import request, flash, jsonify
from flask_login import current_user
from sqlalchemy.orm.exc import NoResultFound

from app.common.exceptions import AppException
from app.common.views.decorators import requires_login
from app.extensions import db
from app.phone_numbers.blueprint import migrations_blueprint
from app.phone_numbers.controllers import user_can_migrate_number
from app.phone_numbers.models import (PhoneNumber, PhoneAction,
                                      RequestStatus)
from app.phone_numbers.review_controllers import review_keep_phone

logger = logging.getLogger(__name__)


@migrations_blueprint.route('/save/', methods=["POST"])
@requires_login
def save_review_form():
    """
    Performed to retrieve delegates: users and egroups
    :return: Response with json content
    """
    logger.debug(request.json)
    logger.debug(current_user)

    """
    {'isPresent': False, 'isPersonal': True, 'wantToKeep': False, 'phone': '74311', 'actionToTake': ''}
    """

    logger.debug(request.json)

    phone_number = request.json.get("phone", None)
    is_present = request.json.get("isPresent", None)
    is_personal = request.json.get("isPersonal", None)
    want_to_keep = request.json.get("wantToKeep", None)
    action_to_take = request.json.get("actionToTake", None)
    has_ip_socket = request.json.get("hasIpSocket", False)
    owner = request.json.get("owner", None)

    try:
        if has_ip_socket.lower() in ('true', 'false'):
            has_ip_socket = True if has_ip_socket.lower() == 'true' else False
        else:
            has_ip_socket = False
    except AttributeError:
        has_ip_socket = False

    if not current_user.is_authenticated:
        raise AppException("Current user must be logged in in order to review a phone number")

    if not user_can_migrate_number(phone_number):
        return jsonify(error={"result": "User {} review phone {}".format(current_user.username, phone_number)})

    try:
        phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()

        phone.processed = True
        phone.is_personal = is_personal
        phone.is_present = is_present
        phone.want_to_keep = want_to_keep
        phone.has_ip_socket = has_ip_socket

        if not is_present:
            logger.info("Phone {} is not present".format(phone_number))
            phone.action_to_take = PhoneAction.DOESNT_EXIST
            phone.request_status = RequestStatus.PENDING_DELETE

            if phone.completed:
                return jsonify(error={"result": "The selected phone was already migrated"})

        else:
            if want_to_keep:
                logger.info("Phone {} wants to be kept".format(phone_number))
                action_to_take = PhoneAction(action_to_take)
                able_to_migrate, message = review_keep_phone(is_personal=is_personal,
                                                             owner_username=owner['username'],
                                                             personal_action=action_to_take,
                                                             phone_number=phone.phone_number)
                if not able_to_migrate:
                    return jsonify(error={
                        "result": "WARNING: Cannot add phone {} to user {}. {}".format(phone.phone_number,
                                                                                       owner["username"],
                                                                                       message)})
            else:
                logger.info("Phone {} doesnt want to be kept".format(phone_number))
                phone.request_status = RequestStatus.PENDING_DELETE
                phone.action_to_take = PhoneAction.REMOVE

                if phone.completed:
                    return jsonify(error={"result": "Phone {} was already migrated".format(phone.phone_number)})

        phone.request_date = datetime.datetime.now()
        phone.request_user = current_user
        db.session.commit()

    except NoResultFound:
        return jsonify(error={"result": "Phone {} doesn't exist".format(phone_number)})

    flash('Phone {} reviewed successfully'.format(phone_number), 'green')
    logger.info("Phone {} reviewed successfully".format(phone_number))
    return jsonify(results={'result': 'Phone {} reviewed successfully'.format(phone_number)})


@migrations_blueprint.route('/migrate/', methods=["POST"])
@requires_login
def migrate_form():
    """
    Performed to retrieve delegates: users and egroups
    :return: Response with json content
    """
    logger.info("Migrating a phone number")

    logger.debug(request.json)
    logger.debug(current_user)

    phone_number = request.json.get("phone", None)

    if not user_can_migrate_number(phone_number):
        return jsonify(
            error={"result": "User {} cannot migrate the phone {}".format(current_user.username, phone_number)})

    try:
        phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()

        if phone.completed:
            return jsonify(error={"result": "The phone {} was already migrated".format(phone_number)})

        if phone.action_to_take == PhoneAction.IP_PHONE or phone.action_to_take == PhoneAction.APPLICATION:
            able_to_migrate = process_keep_phone(is_personal=phone.is_personal,
                                                 owner_username=phone.owner.username,
                                                 personal_action=phone.action_to_take,
                                                 phone_number=phone.phone_number,
                                                 trigger_username=current_user.username)
            if not able_to_migrate:
                return jsonify(error={
                    "result": "WARNING: Cannot add phone {} to user {}. {}".format(phone.phone_number,
                                                                                   phone.owner.username,
                                                                                   "Unable to migrate the selected phone")})
            # phone.request_status = RequestStatus.COMPLETE
        else:
            phone.request_status = RequestStatus.PENDING_DELETE

        # phone.completed = True
        # phone.request_completed_date = datetime.datetime.now()

        db.session.commit()

    except NoResultFound:
        return jsonify(error={"result": "The selected phone doesn't exist"})

    flash('Phone {} migrated successfully'.format(phone_number), 'green')
    return jsonify(results={'result': 'Phone {} migrated successfully'.format(phone_number)})
