import logging
from datetime import date

import random
from flask import render_template, request, session, redirect, url_for, current_app, flash
from flask_login import current_user
from sqlalchemy.orm.exc import NoResultFound

from app.app_settings.models import AppSettings
from app.common.views.decorators import requires_login
from app.phone_numbers.stats_controller import get_total_migration_stats, get_roles_department_stats, \
    get_roles_location_stats
from app.roles.controllers import get_outside_roles, get_office_mode_roles
from app.extensions import db
from app.phone_numbers.blueprint import migrations_blueprint
from app.services.cern_ldap import CernLdapClient, user_to_json
from app.users.forms import ImpersonateForm
from app.users.models import User
from app.users.views.local_dev import login_and_log_user

logger = logging.getLogger(__name__)


@migrations_blueprint.route('/', methods=["GET", "POST"])
@requires_login
def index():
    """
    Homepage view
    It includes as well a form to impersonate users of type ImpersonateForm.
    :return: The page template
    """
    settings = AppSettings.query.get(1)

    d0 = date.today()
    d1 = settings.global_deadline
    delta = d1 - d0

    username = current_user.username

    ldap_client = CernLdapClient()
    egroups = ldap_client.get_user_egroups_by_username(username)

    if current_app.config["OFFICE_MODE"]:
        roles = get_office_mode_roles(egroups, username)
        logger.debug(roles)
    else:
        roles = get_outside_roles(egroups, username)

    stats = None
    if current_user.is_admin:
        stats = get_total_migration_stats()

    if session.get("impersonate", False) or not current_user.is_admin:
        if current_app.config["OFFICE_MODE"]:
            stats = get_roles_department_stats()
        else:
            stats = get_roles_location_stats()

    impersonate_form = ImpersonateForm()

    if request.method == 'POST':
        if impersonate_form.validate_on_submit():
            username = impersonate_form.username.data
            ldap_client = CernLdapClient()
            user = ldap_client.get_user_ldap(username)
            if not user:
                flash('Unable to find the selected user', 'red')
                return redirect(url_for("migrations.index"))
            session["impersonate"] = user_to_json(user)

            query = User.query.filter_by(username=username)

            try:
                existing_user = query.one()
                current_app.logger.info(
                    "User {} was found with person_id {}".format(existing_user.username, existing_user.person_id))
            except NoResultFound:
                current_app.logger.debug("Creating new user")
                existing_user = User(username=str(user['cn']).strip(),
                                     person_id=str(user['employeeID']),
                                     email=str(user['mail']).strip().lower(),
                                     last_name=str(user['sn']).strip(),
                                     first_name=str(user['givenName']).strip(),
                                     is_admin=False)

                current_app.logger.debug("Loading ldap for username: {}".format(str(user['cn']).strip()))
                db.session.add(existing_user)
                db.session.commit()

            login_and_log_user(existing_user)

            return redirect(url_for("migrations.index"))

    return render_template('index.html',
                           days=delta.days,
                           deadline=d1.strftime('%d-%m-%Y'),
                           stats=stats,
                           roles=roles,
                           impersonate_form=impersonate_form,
                           egroups=egroups)
