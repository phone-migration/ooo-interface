import logging
from collections import OrderedDict

from flask import render_template, abort, redirect, url_for

from app.app_settings.models import AppSettings
from app.common.views.decorators import requires_login
from app.phone_numbers.blueprint import migrations_blueprint
from app.roles.controllers import get_all_locations_by_user

logger = logging.getLogger(__name__)


@migrations_blueprint.route('/review', methods=["GET"])
@requires_login
def review():
    """
    Review view
    :return: The page template
    """
    locations = get_all_locations_by_user()

    if len(locations) > 0:
        return redirect(url_for('migrations.review_building', building_name=next(iter(locations))))

    return render_template('review.html', buildings=locations)


@migrations_blueprint.route('/review/building/<string:building_name>', methods=["GET"])
@requires_login
def review_building(building_name):
    """
    View displayed when a building is selected
    :return: The page template
    """
    buildings = get_all_locations_by_user()

    try:
        buildings[building_name]
    except KeyError:
        return render_template('review_404.html')

    floors_sorted = get_building_childs_sorted(building_name, buildings)

    return render_template('_floors.html',
                           selected_building=building_name,
                           buildings=buildings,
                           building_floors=floors_sorted)


def get_building_childs_sorted(building_name, buildings):
    selected_building = buildings[building_name]['floors'].items()
    sorted_floors = OrderedDict(sorted(selected_building))
    floors = OrderedDict()
    # Sort the floors
    for floor_name in sorted_floors:
        logger.debug(floor_name)
        floors[floor_name] = OrderedDict()

        # Sor the rooms
        floor_rooms = OrderedDict()
        sorted_rooms = sorted(buildings[building_name]['floors'][floor_name]['rooms'].items())
        for room_name, room_value in sorted_rooms:
            floor_rooms[room_name] = room_value
        floors[floor_name]['rooms'] = floor_rooms
    return floors


@migrations_blueprint.route('/review/building/<string:building_name>/floor/<string:floor_name>/room/<string:room_name>',
                            methods=["GET", "POST"])
@requires_login
def review_room(building_name, floor_name, room_name):
    """
    View displayed when a room is selected.
    It handles the submission of the PhoneActionForm.
    :return: The page template
    """
    buildings = get_all_locations_by_user()
    settings = AppSettings.query.get(1)

    try:
        building = buildings[building_name]
        phones = building['floors'][floor_name]['rooms'][room_name]['phones']
    except KeyError:
        return render_template('review_404.html')

    floors_sorted = get_building_childs_sorted(building_name, buildings)

    # Preparing everything to be used by React
    props = {}
    numbers = []
    room_purpose = ""
    room_purpose_desc = ""
    for phone, values in phones.items():
        logger.debug(values)
        room_purpose = values["purpose"]
        room_purpose_desc = values["purpose_desc"]
        phone_props = {
            "action": "/save/",
            "phoneNumber": phone,
            "location": "{}-{}-{}".format(values['building'], values['floor'], values['room']),
            "reviewed": values['processed'],
            "migrated": values['completed'],
            "present": values['is_present'],
            "keep": values['want_to_keep'],
            "personal": values['is_personal'],
            "actionToTake": values['action_to_take'],
            "socket": values["has_ip_socket"],
            "phoneId": values['id'],
            "owner": values['owner'],
            "totalCalls": values['total_calls'],
            "displayName": values['display_name'],
            "globalDeadline": str(settings.global_deadline)
        }
        numbers.append(phone_props)
        logger.debug(phone_props)
    props["numbers"] = numbers

    return render_template('_room.html',
                           buildings=buildings,
                           selected_building=building_name,
                           selected_floor=floor_name,
                           selected_room=room_name,
                           props=props,
                           room_purpose=room_purpose,
                           room_purpose_desc=room_purpose_desc,
                           building_floors=floors_sorted
                           )
