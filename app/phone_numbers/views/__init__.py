from .index import index
from .review import (review, review_building, review_room)
from .search import (search_users, search_users_egroups, has_personal_phone)
from .migrate import (save_review_form, migrate_form)
