from flask import url_for
from markupsafe import Markup


def phones_formatter(view, context, model, name):
    phones = u""
    size = len(model.phones)
    current = 0
    for phone in model.phones:
        separator = "" if current == size - 1 else ", "
        phones += Markup(
            u"<a href='%s'>%s</a> %s" % (
                url_for('admin.numbers.edit_view', id=phone.id),
                phone,
                separator
            )
        )
        current = current + 1
    return phones


def user_formatter(view, context, model, name):
    users = u""
    size = len(model.responsibles)
    current = 0
    for user in model.responsibles:
        separator = "" if current == size - 1 else ", "
        users += Markup(
            u"<a href='%s'>%s</a> %s" % (
                url_for('admin.user.edit_view', id=user.id),
                user,
                separator
            )
        )
        current = current + 1
    return users
