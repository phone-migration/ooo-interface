import logging

from app.phone_numbers.models import PhoneNumber
from flask_admin.contrib.sqla.filters import FilterLike

from app.common.views.admin import AppModelView
from app.roles.views.admin import role_formatter

logger = logging.getLogger(__name__)


# Create customized model view class
class AppSettingsModelView(AppModelView):
    can_create = False
    can_edit = True
    can_delete = False
    can_view_details = True
    can_export = True


class PhoneNumberModelView(AppModelView):
    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True
    column_searchable_list = ('display_name', 'phone_number','building', 'floor', 'room')
    column_hide_backrefs = False
    column_list = (
        'phone_number', 'location', 'total_calls', 'action_to_take',
        'is_personal', 'want_to_keep', 'is_present', 'owner', 'request_date', 'request_completed_date',
        'request_user', 'request_status', 'has_ip_socket')
    can_export = True

    column_filters = [FilterLike(PhoneNumber.location, 'Location')]


class ResponsibleUserModelView(AppModelView):
    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True
    column_hide_backrefs = False
    column_list = ("username", "email", "first_name", "last_name", "roles")
    column_searchable_list = ("username", "email", "first_name", "last_name")
    can_export = True

    column_formatters = {
        'roles': role_formatter
    }
