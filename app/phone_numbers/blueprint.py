from app.common.views.utils import blueprint_factory

migrations_blueprint = blueprint_factory('migrations', __name__, url_prefix='/')
