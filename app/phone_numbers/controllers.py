import logging

from flask import current_app
from flask_login import current_user

from app.phone_numbers.models import PhoneNumber
from app.roles.controllers import (get_phones_from_department,
                                   get_all_user_locations,
                                   get_user_phones_from_locations)

logger = logging.getLogger(__name__)


def user_can_migrate_number(number):
    if current_user.is_admin:
        return True

    number = PhoneNumber.query.filter_by(phone_number=number).one()

    logger.info("Current user is {}".format(current_user.username))
    if current_app.config["OFFICE_MODE"]:
        phones = get_phones_from_department(current_user.username)
    else:
        buildings, floors, rooms = get_all_user_locations()
        phones = get_user_phones_from_locations(buildings, floors, rooms)

    if number in phones:
        logger.info("Phone {} is in the phones user can review".format(number.phone_number, current_user.username))
        return True
    logger.info("Phone {} is NOT in the phones user can review".format(number.phone_number, current_user.username))
    return False


def has_owner_personal_phone(username, phone_number=None):
    # TODO Check as well Riccardo's API
    logger.debug('Checking if user {} has personal phone number on database'.format(username))

    if phone_number:
        phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
        if phone.owner and phone.owner.username == username:
            return False

    phones = PhoneNumber.query.filter(
        PhoneNumber.owner.has(username=username)).filter_by(is_personal=True).all()
    if len(phones) == 0:
        logger.debug('User {} has no personal numbers on database'.format(username))
        return False
    return True
