from app.extensions import db
from app.phone_numbers.models import PhoneNumber, PhoneOwner


class PhoneNumberMapper:
    @classmethod
    def create_phone_number(cls, phone_number=None,
                            display_name=None,
                            given_name=None,
                            room=None,
                            floor=None,
                            building=None,
                            total_calls=0,
                            commit=False):
        """
        Create a new instance of PhoneNumber
        :param phone_number: The phone number
        :param display_name: The display name
        :param given_name: The given name
        :param total_calls: Number of calls of this phone number
        :param commit: Whether or not the changes will be committed to the db
        :return: The newly created PhoneNumber
        """
        new = PhoneNumber(phone_number=phone_number, display_name=display_name, given_name=given_name,
                          total_calls=total_calls)
        new.room = room
        new.floor = floor
        new.building = building
        if commit:
            db.session.add(new)
            db.session.commit()

        return new


class PhoneOwnerMapper:
    @classmethod
    def create_phone_owner(cls, name=None, organizational_unit=None, username=None, email=None, commit=False):
        """
        Creates a new PhoneOwner instance
        :param name: Name of the PhoneOwner
        :param organizational_unit: The organizational unit (Ex: IT-CDA-IC) the user belongs to
        :param email: Email of the PhoneOwner
        :param username: Username of the PhoneOwner
        :param commit: Whether or not the changes will be committed to the db
        :return: The newly created PhoneOwner
        """
        new = PhoneOwner()
        new.name = name
        new.organizational_unit = organizational_unit
        new.username = username
        new.email = email
        if commit:
            db.session.add(new)
            db.session.commit()
        return new
