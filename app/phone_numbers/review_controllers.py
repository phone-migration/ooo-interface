import logging

from flask import flash
from sqlalchemy.orm.exc import NoResultFound

from app.phone_numbers.controllers import has_owner_personal_phone
from app.phone_numbers.models import PhoneNumber, PhoneType, RequestStatus, PhoneAction, PhoneOwner
from app.phone_numbers.migrate_controllers import create_new_owner
from app.services.fim import user_has_personal_number_in_fim, UCServicesException

logger = logging.getLogger(__name__)

def review_personal_number(owner_username=None, personal_action=None, phone_number=None):
    """
    Processes the action of adding a personal number to the selected user
    :param trigger_username:
    :param owner_username: User's username
    :param personal_action: Selected action for the phone.
    :param phone_number: (String) Phone number
    :return: (True | False) if the creation was successful
    """
    logger.info("Reviewing phone {} as personal number for user {}".format(phone_number, owner_username))
    phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
    phone.type = PhoneType.PERSONAL
    try:
        has_fim_number = user_has_personal_number_in_fim(owner_username)
        if has_fim_number:
            logger.warning("User {} has already a personal number in FIM.".format(owner_username))
            return False

        if has_owner_personal_phone(owner_username, phone_number):
            logger.warning("User {} has already a personal number.".format(owner_username))
            return False
    except UCServicesException as error:
        logger.error(str(error))
        flash(str(error), "error")

    phone.action_to_take = personal_action
    return True


def review_keep_phone(is_personal=None, owner_username=None, personal_action=None, phone_number=None):
    """
    Action taken when the user wants to keep a personal phone
    :param is_personal: (True | False) If the phone number is going to be personal or not
    :param owner_username:  String. Username of the user
    :param personal_action:
    :param phone_number: (String)
    :return:
    """
    phone = PhoneNumber.query.filter_by(phone_number=phone_number).one()
    phone.request_status = RequestStatus.PENDING
    logger.info("Reviewing phone {} to be kept".format(phone_number))
    if is_personal:
        personal_valid = review_personal_number(owner_username=owner_username,
                                                 personal_action=personal_action,
                                                 phone_number=phone.phone_number)
        if not personal_valid:
            message = "User {} already has a personal number".format(owner_username)
            logger.info("{} Is not a valid personal number".format(phone_number))
            return False, message
    else:
        phone.type = PhoneType.SHARED
        phone.action_to_take = personal_action
    try:
        owner = PhoneOwner.query.filter_by(username=owner_username).one()
    except NoResultFound:
        owner = create_new_owner(owner_username)
    logger.info("Adding phone {} to user {} phone numbers".format(phone_number, owner_username))
    phone.owner = owner
    owner.phones.append(phone)
    return True, ""