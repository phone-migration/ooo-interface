import logging
from importlib import import_module

import os
from flask import Flask
from flask import render_template, current_app
from flask_admin import Admin
from flask_cors import CORS
from flask_migrate import Migrate
from werkzeug.contrib.fixers import ProxyFix

from app.app_settings.models import AppSettings
from app.app_settings.views.admin import MyHomeView
from app.common.blueprint import common_blueprint
from app.common.utils.cern_oauth import load_cern_oauth
from app.common.views.admin import AppModelView
from app.extensions import db, assets, cache
from app.phone_numbers.blueprint import migrations_blueprint
from app.phone_numbers.models import PhoneOwner, ResponsibleUser, PhoneNumber
from app.phone_numbers.views.admin import AppSettingsModelView, ResponsibleUserModelView, PhoneNumberModelView
from app.roles.blueprint import roles_blueprint
from app.roles.models import RoleLocation, RoleDepartment, DelegatedUser
from app.roles.views.admin import RoleLocationModelView, RoleDepartmentModelView, DelegatedUserModelView
from app.static_assets import register_assets
from app.users.blueprint import users_blueprint, local_dev_blueprint

logger = logging.getLogger(__name__)


def inject_conf_var():
    """
    Injects all the application's global configuration variables

    :return: A dict with all the environment variables
    """
    return dict(
        #
        # Analytics
        #
        ANALYTICS_GOOGLE_ID=current_app.config["ANALYTICS_GOOGLE_ID"],
        ANALYTICS_PIWIK_ID=current_app.config["ANALYTICS_PIWIK_ID"],
        DEBUG=current_app.config['DEBUG'],
        IS_LOCAL=current_app.config["IS_LOCAL"],
        OFFICE_MODE=current_app.config["OFFICE_MODE"]
    )


def create_database():
    """
    Creates the database if it doesn't exist before the first request.
    :return: app_settings
    """
    with current_app.app_context():
        db.create_all()


def page_not_found(e):
    logger.debug(e)
    return render_template('404.html'), 404


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"]
        )

        app.config["SQLALCHEMY_RECORD_QUERIES"] = False
        app.config["SQLALCHEMY_POOL_SIZE"] = int(os.environ.get('SQLALCHEMY_POOL_SIZE', 5))
        app.config["SQLALCHEMY_POOL_TIMEOUT"] = int(os.environ.get('SQLALCHEMY_POOL_TIMEOUT', 10))
        app.config["SQLALCHEMY_POOL_RECYCLE"] = int(os.environ.get('SQLALCHEMY_POOL_RECYCLE', 120))
        app.config["SQLALCHEMY_MAX_OVERFLOW"] = int(os.environ.get('SQLALCHEMY_MAX_OVERFLOW', 3))
    else:
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + os.path.join(_basedir, 'webapp.db')

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    Migrate(app, db)
    db.app = app


def load_app_blueprints(app):
    """
    Loads all the blueprints, adding the development ones if needed
    :param app: Flask application instance
    :return: A tuple with all the blueprints
    """
    all_blueprints = (
        migrations_blueprint,
        users_blueprint,
        roles_blueprint,
        common_blueprint
    )

    if app.config["IS_LOCAL"]:
        all_blueprints += (local_dev_blueprint,)

    return all_blueprints


def setup_admin(app):
    """
    Initialize Admin views
    """
    admin = Admin(name='Phone Migration Admin', template_mode='bootstrap3', index_view=MyHomeView(),
                  base_template='admin-layout.html')
    admin.init_app(app)
    admin.add_view(AppSettingsModelView(AppSettings, db.session, endpoint='admin.settings', name='Settings'))
    admin.add_view(
        RoleLocationModelView(RoleLocation, db.session, endpoint='admin.role_location', name='Role for Locations'))
    admin.add_view(
        RoleDepartmentModelView(RoleDepartment, db.session, endpoint='admin.role_department',
                                name='Role for Departments'))
    admin.add_view(
        ResponsibleUserModelView(ResponsibleUser, db.session, endpoint='admin.user', name='Responsibles'))
    admin.add_view(
        DelegatedUserModelView(DelegatedUser, db.session, endpoint='admin.delegates', name='Delegates'))
    admin.add_view(
        PhoneNumberModelView(PhoneNumber, db.session, endpoint='admin.numbers', name='Numbers'))
    admin.add_view(
        AppModelView(PhoneOwner, db.session, endpoint='admin.owners', name='Owners'))


def setup_cache(app):
    """
    Initialize the Cache settings
    """

    if app.config['CACHE_ENABLE']:
        app.config["CACHE_CONFIG"] = {'CACHE_TYPE': 'redis',
                                      "CACHE_REDIS_HOST": app.config["CACHE_REDIS_HOST"],
                                      "CACHE_REDIS_PASSWORD": app.config["CACHE_REDIS_PASSWORD"],
                                      "CACHE_REDIS_PORT": app.config["CACHE_REDIS_PORT"]
                                      }

    else:
        app.config["CACHE_CONFIG"] = {'CACHE_TYPE': 'null'}

    cache.init_app(app, config=app.config['CACHE_CONFIG'])


def _initialize_views(application):
    """
    Initializes all the application blueprints
    :param application: Application where the blueprints will be registered
    :return: -
    """
    all_blueprints = load_app_blueprints(application)

    for bp in all_blueprints:
        application.register_blueprint(bp)


def _load_app_models(app):
    """
    Load all the available models
    """
    with app.app_context():
        """
        Add here the model modules to be loaded automatically on the application factory
        """
        db_models_imports = (
            'app.phone_numbers.models',
            'app.roles.models'
        )
        for app_module in db_models_imports:
            import_module(app_module)


def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    CORS(app)

    if app.config.get('USE_PROXY', False):
        app.wsgi_app = ProxyFix(app.wsgi_app)

    _load_app_models(app)

    """
    Enable the CERN Oauth Authentication
    """
    load_cern_oauth(app)

    """
    Register all the static assets and compile them
    """
    assets.init_app(app)
    with app.app_context():
        register_assets(assets)

    setup_database(app)

    _initialize_views(app)

    " Setup cache and admin "
    setup_cache(app)
    setup_admin(app)

    " Setup decorators "
    app.context_processor(inject_conf_var)
    app.before_first_request(create_database)
    app.errorhandler(404)(page_not_found)

    return app
