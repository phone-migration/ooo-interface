import flask_assets
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache

#
# Database
#
db = SQLAlchemy()
#
# Static Assets
#
assets = flask_assets.Environment()

#
# Caching
#
cache = Cache(config={'CACHE_TYPE': 'null'})

