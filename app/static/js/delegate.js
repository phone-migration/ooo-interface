/**
 * Main JS.
 */

$(document).ready(function () {
    $('#username-valid-icon').hide();
    $('#set-button').prop('disabled', true);
    console.debug($('input[name="username"]').val());

    $('input[name="prompt"]').val($('input[name="username"]').val());

    $('.ui.search')
        .search({
            apiSettings: {
                url: '/search/users-egroups/?q={query}'
            },
            searchFields: [
                'displayName',
                'description',
                'username',
                'type'
            ],
            fields: {
                title: 'displayName'
            },
            minCharacters: 3,
            onSelect: function (result, response) {
                console.debug(result);
                $('input[name="valid"]').val('true');
                $('input[name="username"]').val(result.username);
                $('input[name="type"]').val(result.type);
                $('#username-valid-icon').show();
                $('#set-button').prop('disabled', false);

            },
            onResults: function (response) {
                $('input[name="valid"]').val('false');
                $('input[name="username"]').val("");
                $('input[name="type"]').val("");
                $('#username-valid-icon').hide();
                $('#set-button').prop('disabled', true);
            }
        })
    ;
});