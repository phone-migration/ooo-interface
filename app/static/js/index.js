/**
 * Main JS.
 */

$(document).ready(function () {
    $('#username-valid-icon').hide();
    $('#impersonate-button').prop('disabled', true);
    console.debug($('input[name="username"]').val());

    $('input[name="prompt"]').val($('input[name="username"]').val());

    $('.ui.search')
        .search({
            apiSettings: {
                url: '/search/users/?q={query}'
            },
            searchFields: [
                'displayName',
                'description',
                'username',
                'type'
            ],
            fields: {
                title: 'displayName'
            },
            minCharacters: 3,
            onSelect: function (result, response) {
                $('input[name="username"]').val(result.username);
                $('#username-valid-icon').show();
                $('#impersonate-button').prop('disabled', false);

            },
            onResults: function (response) {
                $('input[name="username"]').val("");
                $('#username-valid-icon').hide();
                $('#impersonate-button').prop('disabled', true);
            }
        })
    ;
});