/**
 * Main JS.
 */

$(document).ready(function () {
    $('.ui.dropdown').dropdown();

    // create sidebar and attach to menu open
    $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
    ;

    $('.ui.accordion')
        .accordion()
    ;

    $('.ui.radio.checkbox')
        .checkbox()
    ;

    $('.ui.checkbox')
        .checkbox()
    ;

    $(".help-button").click(function () {
        console.debug("SHOW");
        $('.longer.modal').modal('show');
    });

    $("#select-service-account-button").click(function () {
        let modal_service = $('.service-account.modal').modal({detachable: false});
        modal_service.modal('show');
    });

    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;

    $('.activating.element')
        .popup()
    ;
});