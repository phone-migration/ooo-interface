import logging

from flask_login import current_user

from flask_admin.contrib import sqla
from flask_admin.contrib.sqla.form import AdminModelConverter
from flask_admin.form import BaseForm
from flask_admin.form.fields import Select2Field
from flask_admin.model.form import converts
from requests.compat import basestring
from sqlalchemy.sql.sqltypes import Enum


class EnumField(Select2Field):
    def __init__(self, column, **kwargs):
        assert isinstance(column.type, Enum)

        def coercer(value):
            # coerce incoming value into an enum value
            if isinstance(value, column.type.enum_class):
                return value
            elif isinstance(value, basestring):
                return column.type.enum_class[value]
            else:
                assert False

        super(EnumField, self).__init__(
            choices=[(v, v) for v in column.type.enums],
            coerce=coercer,
            **kwargs)

    def pre_validate(self, form):
        # we need to override the default SelectField validation because it
        # apparently tries to directly compare the field value with the choice
        # key; it is not clear how that could ever work in cases where the
        # values and choice keys must be different types

        for (v, _) in self.choices:
            if self.data == self.coerce(v):
                break
        else:
            raise ValueError(self.gettext('Not a valid choice'))


class CustomAdminConverter(AdminModelConverter):

    def get_form(self, model, base_class=BaseForm, only=None, exclude=None, field_args=None):
        pass

    @converts("sqlalchemy.sql.sqltypes.Enum")
    def conv_enum(self, field_args, **extra):
        return EnumField(column=extra["column"], **field_args)


class DefaultView(sqla.ModelView):
    model_form_converter = CustomAdminConverter


logger = logging.getLogger(__name__)


class AppModelView(DefaultView):
    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.is_admin:
            return True

        return False
