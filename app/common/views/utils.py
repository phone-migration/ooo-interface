from flask import Blueprint


def blueprint_factory(name, import_name, url_prefix='/'):
    """
    Generates blueprint objects for view modules.
     (e.g. 'home.index' for pypi_portal.views.home.index).
    :param url_prefix: URL prefix passed to the blueprint.
    :return: Blueprint instance for a view module.
    """
    templates_folder = 'templates'

    blueprint = Blueprint(name, import_name, url_prefix=url_prefix, template_folder=templates_folder)

    return blueprint
