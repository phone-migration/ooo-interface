from functools import wraps
from flask import redirect, url_for
from flask_login import current_user


def requires_login(f):
    """
    Add this decorator to the views that require a logged in user.

    :param f: Function that will be wrapped with this decorator.
    :return: The decorated function
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for('users.login'))
        return f(*args, **kwargs)

    return decorated_function
