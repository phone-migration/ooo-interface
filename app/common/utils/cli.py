import csv

import click
from flask import current_app
from flask.cli import with_appcontext
from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.phone_numbers.mappers import PhoneNumberMapper
from app.phone_numbers.models import PhoneNumber, ResponsibleUser
from app.phone_numbers.utils import create_initial_settings
from app.roles.controllers import get_all_locations_by_user, get_all_phones_by_user, get_office_mode_roles, \
    get_phones_from_department_role
from app.roles.mappers import RoleLocationMapper, RoleDepartmentMapper
from app.roles.models import RoleLocation, RoleDepartment, RoleType, DelegatedUser, DelegatedType
from app.services.cern_ldap import CernLdapClient


@click.group()
def webapp():
    """Perform webapp operations."""
    pass


@webapp.command()
@with_appcontext
def init_db():
    """
    Initialize the database.
    """
    click.echo('Initializing the db')
    db.create_all()
    create_initial_settings()


@webapp.command()
@with_appcontext
def clear_db():
    """
    Removes all the tables of the database and their content
    """
    click.echo('Clearing the db')
    db.drop_all()
    click.echo('Initializing the db')
    db.create_all()


@webapp.command()
@with_appcontext
@click.option('--csv_file', default='/opt/app-root/src/uploads/office_phones.csv')
def init_office_phones(csv_file):
    if current_app.config["OFFICE_MODE"]:
        init_phones(csv_file)
    else:
        click.echo("You cannot run this command if OFFICE_MODE is disabled")


@webapp.command()
@with_appcontext
@click.option('--csv_file', default='/opt/app-root/src/uploads/outside_phones.csv')
def init_outside_phones(csv_file):
    if current_app.config["OFFICE_MODE"]:
        click.echo("You cannot run this command on OFFICE_MODE")
    else:
        init_phones(csv_file)


def init_phones(csv_file):
    """
    Removes all the tables of the database and their content
    """
    with open(csv_file, 'r', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(spamreader, None)  # We don't want the header
        success = 0
        error = 0
        already_exists = 0
        errors = []
        for row in spamreader:
            phone_data = get_phone_data(row)
            if phone_data['location']['location_error']:
                click.echo('Creating phone in unknown location')
                phone_data['location']['room'] = phone_data['location']['outlet']
                phone_data['location']['floor'] = "-"
                phone_data['location']['building'] = "-"
            try:
                PhoneNumber.query.filter_by(phone_number=str(phone_data['phone_number'])).one()
                already_exists += 1
                click.echo('Phone already exists')
                continue
            except NoResultFound:
                click.echo("Creating new phone: {}".format(phone_data['phone_number']))
                phone = PhoneNumberMapper.create_phone_number(phone_number=phone_data['phone_number'],
                                                              display_name=phone_data['display_name'],
                                                              room=phone_data['location']['room'],
                                                              floor=phone_data['location']['floor'],
                                                              building=phone_data['location']['building'],
                                                              total_calls=phone_data['total_calls'],
                                                              commit=True)
                phone.department = phone_data['department']
                phone.org_unit = phone_data['org_unit']
                phone.purpose = phone_data['purpose']
                db.session.commit()
                click.echo('OK')
            success += 1

        click.echo("---------- Errors ----------")
        for error in errors:
            click.echo("{} - {}: {}".format(error["id"], error["name"], error["message"]))
        click.echo("---------- End Errors ----------")
        click.echo(
            "Phones already imported: success: {} error: {} already exists: {}".format(success, error,
                                                                                       already_exists))


def get_location(location_data, outlet_data):
    location_error = False
    building_name = ''
    floor_name = ''
    room_name = ''
    try:
        building_name = location_data.split("/")[0]
        floor_office = location_data.split("/")[1]
        floor_name = floor_office.split("-")[0]
        room_name = floor_office.split("-")[1]
    except IndexError:
        """
        If we cannot get the data from the expected field, we will try to get it from
        outlet_data
        """
        try:
            location = outlet_data.split(" ")
            building_name = str(int(location[1].strip()))
            floor_office = location[2].split("-")
            floor_name = floor_office[0].strip()
            room_name = floor_office[1].strip()
        except (IndexError, ValueError):
            location_error = True
    except ValueError:
        location_error = True

    return {
        'building': building_name,
        'floor': floor_name,
        'room': room_name,
        'outlet': outlet_data,
        'location_error': location_error
    }


def get_phone_data(row):
    phone_number = row[6].strip()
    purpose = row[5].strip()
    location = get_location(row[0].strip(), row[1].strip())
    display_name = row[7].strip()
    department = row[3].strip()
    org_unit = row[2].strip()
    total_calls = int(row[4].strip()) if row[4].strip() is not "" else 0

    return {
        'phone_number': phone_number,
        'purpose': purpose,
        'location': location,
        'department': department,
        'org_unit': org_unit,
        'display_name': display_name,
        'total_calls': total_calls
    }


@webapp.command()
@with_appcontext
@click.option('--csv_file', default='/opt/app-root/src/uploads/office_users.csv')
def init_office_resp(csv_file):
    if not current_app.config["OFFICE_MODE"]:
        click.echo("You cannot run this command if OFFICE_MODE is disabled")
        return -1

    with open(csv_file, 'r', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(spamreader, None)  # We don't want the header
        success = 0
        error = 0
        already_exists = 0
        errors = []
        existing = []
        for row in spamreader:
            pass
            person_id = row[0].strip()  # 0 person_id
            department = row[5].strip()  # 5 department

            # RoleDepartmentMapper.create_role_department()
            # get the user from LDAP
            ldap_client = CernLdapClient()
            ldap_user = ldap_client.get_user_ldap_by_person_id(person_id)

            if not ldap_user:
                error += 1
                errors.append({'id': person_id,
                               'name': '',
                               'message': 'Not found on LDAP'})
                continue

            query = ResponsibleUser.query.filter_by(person_id=str(ldap_user['employeeID']))
            try:
                new_user = query.one()
                already_exists += 1
                existing.append({'id': str(ldap_user['employeeID']),
                                 'name': str(ldap_user['givenName']) + " " + str(ldap_user['sn'])})
            except NoResultFound:
                new_user = ResponsibleUser(username=str(ldap_user['cn']).strip(),
                                           person_id=str(ldap_user['employeeID']),
                                           email=str(ldap_user['mail']).strip().lower(),
                                           last_name=str(ldap_user['sn']).strip(),
                                           first_name=str(ldap_user['givenName']).strip()
                                           )
                db.session.add(new_user)
                success += 1
            # # Create a Role Deparment with this user as responsible
            query = RoleDepartment.query.filter_by(department=department)
            try:
                role_department = query.one()
            except NoResultFound:
                click.echo(department)
                role_department = RoleDepartmentMapper.create_role_department(department=department, commit=True)

            if new_user not in role_department.responsibles:
                role_department.responsibles.append(new_user)
            db.session.commit()

        click.echo("---------- Errors ----------")
        for error in errors:
            click.echo("{} - {}: {}".format(error["id"], error["name"], error["message"]))
        click.echo("---------- Already Exist ----------")
        for existing_user in existing:
            click.echo("{} - {}".format(existing_user["id"], existing_user["name"]))
        click.echo("---------- End Errors ----------")
        click.echo(
            "Users already imported: success: {} error: {} already exists: {}".format(success, error,
                                                                                      already_exists))
    return 0


@webapp.command()
@with_appcontext
@click.option('--csv_file', default='/opt/app-root/src/uploads/outside_users.csv')
def init_outside_resp(csv_file):
    if current_app.config["OFFICE_MODE"]:
        click.echo("You cannot run this command on OFFICE_MODE")
        return -1

    # "Holder","Org. Unit","Role Type","1st Target","1st Target Type","2nd Target","2nd Target Type","Priority"
    with open(csv_file, 'r', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(spamreader, None)  # We don't want the header
        loc_responsibles = []
        dtsos = []
        tsos = []
        dep_phone_managers = []
        for row in spamreader:
            try:
                person_id = row[1]
                person_name = row[2]
                role_type = row[4]
                location_type = row[5]
                location_name = row[6]

                building_name = location_name.split("-")[0]
                try:
                    floor_name = location_name.split("-")[1]
                except IndexError:
                    floor_name = None

                try:
                    room_name = location_name.split("-")[2]
                except IndexError:
                    room_name = None

                click.echo("New row")
                click.echo("{} {} {}".format(location_name, role_type, location_type))

                new = {
                    "person_id": person_id,
                    "role_type": role_type,
                    "person_name": person_name,
                    "location_type": location_type,
                    "location_name": location_name,
                    "building_name": building_name,
                    "floor_name": floor_name,
                    "room_name": room_name,
                }

                # if current_app.config["OFFICE_MODE"]:
                #     dep_phone_managers.append(new)
                # else:
                if role_type == "LOC_RESP":
                    loc_responsibles.append(new)
                if role_type == "DTSO":
                    dtsos.append(new)
                if role_type == "TSO":
                    tsos.append(new)
            except IndexError:
                continue

        import_responsibles(loc_responsibles, RoleType.LOC_RESP)
        import_responsibles(dtsos, RoleType.DTSO)
        import_responsibles(tsos, RoleType.TSO)

    return 0


def import_responsibles(responsibles, role_type):
    for resp in responsibles:
        click.echo("Processing resp: {}".format(resp["person_id"]))
        click.echo(resp["location_name"])
        click.echo(resp["location_type"])
        # Find a location with location_name
        # get the user from LDAP
        ldap_client = CernLdapClient()
        ldap_user = ldap_client.get_user_ldap_by_person_id(resp["person_id"])
        if ldap_user:
            query = ResponsibleUser.query.filter_by(person_id=str(ldap_user['employeeID']))
            try:
                new_user = query.one()
            except NoResultFound:
                new_user = ResponsibleUser(username=str(ldap_user['cn']).strip(),
                                           person_id=str(ldap_user['employeeID']),
                                           email=str(ldap_user['mail']).strip().lower(),
                                           last_name=str(ldap_user['sn']).strip(),
                                           first_name=str(ldap_user['givenName']).strip()
                                           )
                db.session.add(new_user)
            # # Create a Role Location with this user as responsible
            query = RoleLocation.query.filter_by(role=role_type,
                                                 building=resp["building_name"],
                                                 floor=resp["floor_name"],
                                                 room=resp["room_name"])
            try:
                role_location = query.one()
            except NoResultFound:
                role_location = RoleLocationMapper.create_role_location(role=role_type,
                                                                        building=resp["building_name"],
                                                                        floor=resp["floor_name"],
                                                                        room=resp["room_name"],
                                                                        commit=True)

            # If the location has a LOC_RESP, then don't add a TSO or DTSO
            add_responsible = True
            if role_type in [RoleType.DTSO, RoleType.TSO]:
                try:
                    RoleLocation.query.filter_by(role=RoleType.LOC_RESP,
                                                 building=resp["building_name"],
                                                 floor=resp["floor_name"],
                                                 room=resp["room_name"]).one()
                    add_responsible = False
                except NoResultFound:
                    pass

            if add_responsible:
                role_location.responsibles.append(new_user)
        else:
            click.echo("ERROR: User {0} was not found on LDAP".format(resp["person_id"]))
        # If not found continue

        click.echo("{} LOC_RESP".format(len(responsibles)))


@webapp.command()
@with_appcontext
def echo_command():
    """
    Initialize the database.
    """
    click.echo('Testing is ok')
    return 0


@webapp.command()
@with_appcontext
def export_pending_departments():
    """
    Initialize the database.
    """
    if not current_app.config["OFFICE_MODE"]:
        click.echo("You must run this command on OFFICE_MODE")
        return -1

    roles_for_departments = RoleDepartment.query.all()
    users = []
    count = 10
    for role in roles_for_departments:

        for responsible in role.responsibles:
            user_phones = get_all_phones_by_user(responsible.username)
            for phone in user_phones:
                if not phone.processed:
                    users.append(responsible)
                    break

    delegates = DelegatedUser.query.all()
    egroups = []
    for delegate in delegates:
        if delegate.type == DelegatedType.USER:
            user_phones = get_all_phones_by_user(delegate.name)
            for phone in user_phones:
                if not phone.processed:
                    user = CernLdapClient().get_user_ldap(delegate.name)
                    responsible = ResponsibleUser(username=user["cn"], person_id=user["employeeID"],
                                                  email=user["mail"])
                    users.append(responsible)
                    break

        if delegate.type == DelegatedType.EGROUP:
            for role in delegate.dep_roles:
                phones = get_phones_from_department_role(role.department)
                for phone in phones:
                    if not phone.processed:
                        class Egroup:
                            pass
                        egroup = Egroup()
                        egroup.name = delegate.name
                        egroups.append(egroup)
                        break

    print(egroups)
    remaining_users_file = "{}/remaining_users_departments.csv".format(current_app.config["GENERATED_CSV_PATH"])
    with open(remaining_users_file, 'w', newline='') as csvfile:

        spam_writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for user in set(users):
            spam_writer.writerow(['P', user.username, user.email, user.person_id])

        for egroup in set(egroups):
            spam_writer.writerow(['S', egroup.name])


@webapp.command()
@with_appcontext
def export_pending_locations():
    """
    Initialize the database.
    """
    if current_app.config["OFFICE_MODE"]:
        click.echo("You cannot run this command on OFFICE_MODE")
        return -1

    roles_for_locations = RoleLocation.query.all()
    users = []
    egroups = []
    count = 10
    for role in roles_for_locations:
        for responsible in role.responsibles:
            user_phones = get_all_phones_by_user(responsible.username)
            for phone in user_phones:
                if not phone.processed:
                    users.append(responsible)
                    break

    delegates = DelegatedUser.query.all()
    for delegate in delegates:
        if delegate.type == DelegatedType.USER:
            user_phones = get_all_phones_by_user(delegate.name)
            for phone in user_phones:
                if not phone.processed:
                    user = CernLdapClient().get_user_ldap(delegate.name)
                    responsible = ResponsibleUser(username=user["cn"], person_id=user["employeeID"],
                                                  email=user["mail"])
                    users.append(responsible)
                    break

        if delegate.type == DelegatedType.EGROUP:
            for role in delegate.roles:
                phones = PhoneNumber.query.filter_by(building=role.building, floor=role.floor, room=role.room).all()
                for phone in phones:
                    if not phone.processed:
                        class Egroup:
                            pass

                        egroup = Egroup()
                        egroup.name = delegate.name
                        egroups.append(egroup)
                        break

    users = set(users)
    remaining_users_file = "{}/remaining_users_locations.csv".format(current_app.config["GENERATED_CSV_PATH"])
    with open(remaining_users_file, 'w', newline='') as csvfile:

        spam_writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for user in users:
            spam_writer.writerow(['P', user.username, user.email, user.person_id])

        for egroup in set(egroups):
            spam_writer.writerow(['S', egroup.name])

    """
    Type of the member (P, person; A, account; E, external email, S and D, static and dynamic egroup)
    The name of the account/egroup.
    The email associated to the account/egroup
    The person ID of the owner of the resource (account/egroup)
    The name of the owner
    The primary email of the owner.
    """

    return 0


def initialize_cli(app):
    """
    Add additional commands to the CLI. These are loaded automatically on the main.py

    :param app: App to attach the cli commands to
    :return: None
    """
    app.cli.add_command(init_db)
    app.cli.add_command(clear_db)
    app.cli.add_command(init_office_phones)
    app.cli.add_command(init_outside_phones)
    app.cli.add_command(init_outside_resp)
    app.cli.add_command(init_office_resp)
    app.cli.add_command(export_pending_locations)
    app.cli.add_command(export_pending_departments)
