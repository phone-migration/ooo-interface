from flask import current_app
from flask_dance import OAuth2ConsumerBlueprint
from flask_dance.consumer import oauth_authorized
from flask_dance.consumer.backend.sqla import SQLAlchemyBackend
from flask_login import (
    LoginManager, current_user
)
from sqlalchemy.exc import InternalError
from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db, cache
from app.users.models import User, OAuth

from app.users.views.local_dev import login_and_log_user

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack
from flask.globals import LocalProxy, _lookup_app_object
from functools import partial

import logging
logger = logging.getLogger(__name__)

cern = LocalProxy(partial(_lookup_app_object, "cern_oauth"))


def make_cern_blueprint(client_id=None, client_secret=None, redirect_url=None, redirect_to=None, login_url=None,
                        backend=None):
    """
    Generates the cern oauth blueprint
    
    :param login_url: URL for login
    :param redirect_to: URL to redirect to
    :param client_id: CERN Oauth client ID
    :param client_secret: CERN Oauth secret ID
    :param redirect_url: CERN Oauth Redirect URL
    :param backend: Backend to use
    :return: 
    """
    cern_bp = OAuth2ConsumerBlueprint('cern', __name__,
                                      url_prefix='/oauth',
                                      # oauth specific settings
                                      token_url="https://oauth.web.cern.ch/OAuth/Token",
                                      authorization_url="https://oauth.web.cern.ch/OAuth/Authorize",
                                      # local urls
                                      login_url=login_url,
                                      authorized_url='/cern/authorized',
                                      client_id=client_id,
                                      client_secret=client_secret,
                                      backend=backend,
                                      redirect_to=redirect_to,
                                      redirect_url=redirect_url
                                      )

    cern_bp.from_config["client_id"] = "CERN_OAUTH_CLIENT_ID"
    cern_bp.from_config["client_secret"] = "CERN_OAUTH_CLIENT_SECRET"

    @cern_bp.before_app_request
    def set_applocal_session():
        ctx = stack.top
        ctx.cern_oauth = cern_bp.session

    return cern_bp


def load_cern_oauth(app):
    """
    Loads the CERN Oauth into the application
    
    :param app: The Flask app instance to apply the blueprint
    :return: -
    """
    auth_backend = SQLAlchemyBackend(OAuth, db.session, user=current_user)
    cern_bp = make_cern_blueprint(client_id=app.config['CERN_OAUTH_CLIENT_ID'],
                                  client_secret=app.config['CERN_OAUTH_CLIENT_SECRET'],
                                  backend=auth_backend,
                                  login_url="/cern")
    app.register_blueprint(cern_bp)

    # setup login manager
    login_manager = LoginManager()
    login_manager.login_view = 'cern.login'

    @login_manager.user_loader
    def load_user(user_id):
        try:
            return User.query.get(int(user_id))
        except InternalError as e:
            current_app.logger.warning(str(e))
            return None

    login_manager.init_app(app)

    @oauth_authorized.connect_via(cern_bp)
    def cern_logged_in_signal(bp, token):
        cern_logged_in(bp, token)


def cern_logged_in(cern_bp, token):
    """
    Triggered when a user has logged in using the CERN Oauth
    :param cern_bp: CERN oauth blueprint (Not used)
    :param token: Oauth token (Not used)
    :return: -
    """
    response = cern.get('https://oauthresource.web.cern.ch/api/User')
    response.raise_for_status()
    data = response.json()
    is_admin = False

    query = User.query.filter_by(username=data['username'].strip())

    try:
        existing_user = query.one()
        current_app.logger.info(
            "User {} was found with person_id {}".format(existing_user.username, existing_user.person_id))
    except NoResultFound:
        current_app.logger.debug("Creating new user")
        existing_user = User(username=data['username'].strip(),
                             person_id=data['personid'],
                             email=data['email'].strip(),
                             last_name=data['last_name'].strip(),
                             first_name=data['first_name'].strip(),
                             is_admin=is_admin)

        db.session.add(existing_user)
        db.session.commit()

    login_and_log_user(existing_user)

    egroups_info = get_user_egroups(existing_user.id)

    admin_egroup_found = False
    for egroup_name in egroups_info.json()['groups']:
        # Check if the user is admin
        if egroup_name in [egroup.strip() for egroup in current_app.config['CERN_OAUTH_ADMIN_EGROUPS'].split(",")]:
            admin_egroup_found = True
            existing_user.is_admin = True
    if not admin_egroup_found:
        existing_user.is_admin = False

    db.session.commit()

    current_app.logger.info('OAuth login successful for %s (%s #%d)', data['username'], data['email'],
                            data['personid'])


@cache.memoize(timeout=600)
def get_user_egroups(user_id):
    """
    Retrieves the egroups of the user from the ouath api or from the cache

    :param user_id: Required by memoize to distinguish the egroups to be obtained. it has to be an integer
    :return: The result of the request
    """
    egroups_info = []
    if user_id:
        egroups_info = cern.get('https://oauthresource.web.cern.ch/api/Groups')
    return egroups_info
