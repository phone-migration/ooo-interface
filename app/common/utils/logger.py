from logging.handlers import RotatingFileHandler
from datetime import datetime
import logging
from pytz import timezone, utc
import sys


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


formatter = logging.Formatter(
    '%(levelname)s - %(asctime)s - %(name)s - %(message)s - %(pathname)s - %(funcName)s():%(lineno)d')


def setup_webapp_logs(to_file=True, webapp_log_path="", log_level="DEV", send_mail=False, mail_configuration=None):
    """
    Configures the logs of the application
    :param to_file: (True|False)
    :param webapp_log_path: Absolute path for the logs file
    :param log_level: (DEV|PROD)
    :param send_mail: (True|False) Whether or not to send a mail if an error happens
    :param mail_configuration: dict with the SMTP server configuration
    :return: -
    """
    logger = logging.getLogger("app")

    if log_level == 'DEV':
        logger.setLevel(logging.DEBUG)

    if log_level == 'PROD':
        logger.setLevel(logging.INFO)

    logging.Formatter.converter = zurich_time

    configure_stdout_logging(logger=logger, log_level=log_level)

    if to_file:
        print("Logging to file -> True")
        configure_file_logging(logger=logger, file_path=webapp_log_path, log_level=log_level)

    if send_mail:
        configure_email_logging(logger=logger, mail_configuration=mail_configuration)


def configure_file_logging(logger=None, file_path=None, log_level="DEV"):
    """
    Configures the logging to a file
    :param logger: Logger instance to be configured
    :param file_path: Absolute path to the log file
    :param log_level: (DEV|PROD)
    :return: -
    """
    unable_to_create_log_file = False
    handler = None
    try:
        handler = logging.handlers.TimedRotatingFileHandler(file_path, when='W6', backupCount=5)
        handler.setFormatter(formatter)
    except Exception as e:
        unable_to_create_log_file = True
        print("It seems there is a problem creating the log file. Logs will be only on stdout. Error: {}".format(e))

    if handler:
        if log_level == 'DEV':
            handler.setLevel(logging.DEBUG)

        if log_level == 'PROD':
            handler.setLevel(logging.INFO)

        if not unable_to_create_log_file:
            logger.addHandler(handler)


def configure_stdout_logging(logger=None, log_level="DEV"):
    """
    Configure the logging to stdout
    :param logger: Logger instance to be configured
    :param log_level: (DEV|PROD)
    :return: -
    """
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == 'DEV':
        stream_handler.setLevel(logging.DEBUG)
    if log_level == 'PROD':
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)


def configure_email_logging(logger=None, mail_configuration=None):
    """
    Configures the logging to email
    :param logger: Logger instance to be configured
    :param mail_configuration: dict with the SMTP configuration
    :return: -
    """
    if mail_configuration is None:
        mail_configuration = {}

    fmt_email = logging.Formatter("""
            Message type:  %(levelname)s
            Name:          %(name)s
            Location:      %(pathname)s:%(lineno)d
            Module:        %(module)s/%(filename)s
            Function:      %(funcName)s
            Time:          %(asctime)s
            Message:

            %(message)s
        """)

    # email in case of errors
    mail_handler = logging.handlers.SMTPHandler(mail_configuration.get('mail_hostname'),
                                                mail_configuration.get('mail_from'),
                                                mail_configuration.get('mail_to'), "Web webapp error")
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(fmt_email)
    logger.addHandler(mail_handler)
