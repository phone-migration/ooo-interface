from app.common.views.utils import blueprint_factory

common_blueprint = blueprint_factory('common', __name__, url_prefix='/')
