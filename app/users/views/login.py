from flask import render_template, redirect, url_for, session
from flask_login import login_required, logout_user

from app.users.blueprint import users_blueprint


@users_blueprint.route('/logout')
@login_required
def logout():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    session["impersonate"] = None
    logout_user()
    return redirect(url_for('migrations.index'))


@users_blueprint.route('/login')
def login():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    return render_template('login.html')
