from sqlalchemy.orm.exc import NoResultFound

from flask import redirect, url_for, current_app
from flask_login import login_user

from app.extensions import db
from app.users.blueprint import local_dev_blueprint
from app.users.models import User


@local_dev_blueprint.route('/login-admin')
def login_admin():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    query = User.query.filter_by(username="admin")

    try:
        user = query.one()
    except NoResultFound:
        user = User("admin",
                    person_id=1,
                    email="testadmin@cern.ch",
                    last_name="Macklin Admin",
                    first_name="Burt",
                    is_admin=True)

        db.session.add(user)
        db.session.commit()

    return login_and_redirect_user(user)


@local_dev_blueprint.route('/login-normal')
def login_normal_user():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    query = User.query.filter_by(username="fernandor2")
    try:
        user = query.one()
    except NoResultFound:
        user = User("fernandor2",
                    person_id=2,
                    email="testnotadmin@cern.ch",
                    last_name="Macklin",
                    first_name="Burt",
                    is_admin=False)

        db.session.add(user)
        db.session.commit()

    return login_and_redirect_user(user)


def login_and_redirect_user(user):
    """
    Logs the user and writes to the log
    :param user: User instance
    :return: A redirection to the Home page
    """
    login_and_log_user(user)
    return redirect(url_for('migrations.index'))


def login_and_log_user(user):
    """
    Logs the user using flask_login and prints to the log
    :param user: Instance of User
    :return: -
    """
    if login_user(user):
        current_app.logger.info(
            "Login user {} with person_id: {}".format(user.username, user.person_id))
