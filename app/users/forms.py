from flask_wtf import FlaskForm
from wtforms import validators, HiddenField, SubmitField


class UsernameForm(FlaskForm):
    username = HiddenField('Username', default="", validators=[validators.DataRequired()])
    valid = HiddenField('Phone id', default="false", validators=[validators.DataRequired()])
    type = HiddenField('Type', default="", validators=[validators.DataRequired()])


class ImpersonateForm(FlaskForm):
    username = HiddenField('Username', default="", validators=[validators.DataRequired()])


class CancelImpersonateForm(FlaskForm):
    impersonate = SubmitField('Cancel Impersonation', validators=[validators.DataRequired()])
