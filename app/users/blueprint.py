from app.common.views.utils import blueprint_factory

users_blueprint = blueprint_factory('users', __name__, url_prefix='/')
local_dev_blueprint = blueprint_factory('local_dev', __name__, url_prefix='/')
