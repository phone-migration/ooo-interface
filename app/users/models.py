import datetime

from flask_login import UserMixin
from flask_dance.consumer.backend.sqla import OAuthConsumerMixin

from app.common.models import ModelBase
from app.extensions import db


class BaseUser(ModelBase, UserMixin):
    __abstract__ = True
    __table_args__ = (db.CheckConstraint('email = lower(email)', 'lowercase_email'),)

    person_id = db.Column(db.Integer, unique=True)
    first_name = db.Column(db.String(255))
    username = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    department = db.Column(db.String(255), nullable=True)
    group = db.Column(db.String(255), nullable=True)
    section = db.Column(db.String(255), nullable=True)
    organization = db.Column(db.String(255), nullable=True)
    building = db.Column(db.String(255), nullable=True)

    active = db.Column(db.Boolean, default=True)
    confirmed_at = db.Column(db.DateTime, default=datetime.datetime.now)

    def __init__(self, username=None, person_id=None, email=None, last_name=None, first_name=None, is_admin=False):
        self.username = username
        self.person_id = person_id
        self.email = email
        self.last_name = last_name
        self.first_name = first_name
        self.is_admin = is_admin

    @property
    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def department_and_group(self):
        return '{}-{}'.format(self.department, self.group)

    def __str__(self):
        return """{} {} ({} {})""".format(self.first_name, self.last_name, self.username,
                                          self.person_id)

    def __repr__(self):
        admin = ', is_admin=True' if self.is_admin else ''
        return '{} ({}, {} {}) {})'.format(self.name, self.username, self.email, self.person_id, admin)

    def to_json(self):
        return {'id': self.id, 'first_name': self.first_name, 'last_name': self.last_name, 'email': self.email,
                'is_admin': self.is_admin}


class User(BaseUser):
    """
    Represents an application User
    """
    __tablename__ = 'user'


class OAuth(db.Model, OAuthConsumerMixin):
    """
    Represents an Oauth connection in the application
    """
    __tablename__ = 'flask_dance_oauth'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)
