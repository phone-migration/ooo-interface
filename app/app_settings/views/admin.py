from datetime import date

from app.app_settings.models import AppSettings
from flask_admin import AdminIndexView, expose

from app.phone_numbers.stats_controller import get_total_migration_stats


class MyHomeView(AdminIndexView):
    """
    View for the Admin dashboard home
    """
    @expose('/')
    def index(self):
        """
        View for the admin dashboard home
        :return: The template for the page with its variables
        """
        settings = AppSettings.query.get(1)

        d0 = date.today()
        d1 = settings.global_deadline
        delta = d1 - d0

        stats = get_total_migration_stats()

        return self.render('admin/home.html', days=delta.days, stats=stats)
