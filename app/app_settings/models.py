import datetime

from dateutil.relativedelta import relativedelta

from app.common.models import ModelBase
from app.extensions import db


class AppSettings(ModelBase):
    """
    Represents an application User
    """
    __tablename__ = 'app_settings'

    # Dates
    global_deadline = db.Column(db.Date, default=datetime.datetime.today)

    def __init__(self):
        self.global_deadline = datetime.datetime.today() + relativedelta(months=1)
