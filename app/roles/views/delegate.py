from flask_login import current_user

from app.common.exceptions import AppException
from app.extensions import db
from sqlalchemy.orm.exc import NoResultFound

from app.roles.blueprint import roles_blueprint
from app.roles.mappers import DelegatedUserMapper
from app.users.forms import UsernameForm
from flask import render_template, request, redirect, url_for, flash, current_app

from app.roles.models import RoleDepartment, RoleLocation, DelegatedType, DelegatedUser
from app.services.cern_ldap import CernLdapClient
from app.common.views.decorators import requires_login

import logging

logger = logging.getLogger(__name__)


@roles_blueprint.route('/delegate/<int:role_id>', methods=["GET", "POST"])
@requires_login
def delegate(role_id):
    """
    View for the delegation page.
    It contains a form to set up the delegates of type UsernameForm
    :return: The page template
    """
    if current_app.config["OFFICE_MODE"]:
        role = RoleDepartment.query.get(role_id)
    else:
        role = RoleLocation.query.get(role_id)
    if not current_user.is_admin and current_user.username not in [responsible.username for responsible in
                                                                   role.responsibles]:
        return redirect(url_for('migrations.index'))

    form = UsernameForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            valid = form.valid.data
            delegate_type = form.type.data
            username = form.username.data
            ldap_client = CernLdapClient()
            if delegate_type == 'user':
                result = ldap_client.get_user_ldap(username)
            elif delegate_type == 'egroup':
                result = ldap_client.get_egroup_by_name(username)
            else:
                raise AppException("The username type is invalid")

            delegate_type = DelegatedType.USER if delegate_type == 'user' else DelegatedType.EGROUP
            if result and valid == 'true':
                try:
                    existing_delegate = DelegatedUser.query.filter_by(name=username, type=delegate_type).one()
                except NoResultFound:
                    existing_delegate = DelegatedUserMapper.create_delegated_user(name=username,
                                                             delegated_type=delegate_type,
                                                             commit=True)
                role.delegate = existing_delegate
                if current_app.config["OFFICE_MODE"]:
                    existing_delegate.dep_roles.append(role)
                else:
                    existing_delegate.roles.append(role)
                db.session.commit()
                flash('Delegate set successfully', 'green')
                return redirect(url_for('roles_bp.delegate', role_id=role.id))

        flash('Unable to set the delegate', 'red')
        return redirect(url_for('roles_bp.delegate', role_id=role.id))
    else:
        if role and role.delegate:
            form.username.default = role.delegate.name
            form.process()

    return render_template('delegate.html', role=role, form=form)
