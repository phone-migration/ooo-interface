import logging

from flask import url_for
from markupsafe import Markup

logger = logging.getLogger(__name__)


def role_formatter(view, context, model, name):
    users = u""
    size = len(model.roles)
    current = 0
    for user in model.roles:
        separator = "" if current == size - 1 else ", "
        users += Markup(
            u"<a href='%s'>%s</a> %s" % (
                url_for('admin.role_location.edit_view', id=user.id),
                user,
                separator
            )
        )
        current = current + 1
    return users


def delegate_formatter(view, context, model, name):
    return Markup(
        u"<a href='%s'>%s</a>" % (
            url_for('admin.delegates.edit_view', id=model.delegate.id),
            model.delegate)
    ) if model.delegate else ""
