import logging

from app.common.views.admin import AppModelView
from app.phone_numbers.views.formatters import user_formatter

from app.roles.models import DelegatedType
from app.roles.views.formatters import delegate_formatter, role_formatter
from app.services.cern_ldap import CernLdapClient

logger = logging.getLogger(__name__)


# Create customized model view class
class RoleLocationModelView(AppModelView):
    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True
    # inline_models = (DelegatedUser,)
    column_list = ("role", "building", "floor", "room", "responsibles", "delegate")
    column_searchable_list = ('role', "building", "room")
    column_hide_backrefs = False
    can_export = True

    column_formatters = {
        'responsibles': user_formatter,
        'delegate': delegate_formatter
    }


# Create customized model view class
class RoleDepartmentModelView(AppModelView):
    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True
    # inline_models = (DelegatedUser,)
    column_list = ("role", "department", "responsibles", "delegate")
    column_searchable_list = ('role',)
    column_hide_backrefs = False
    can_export = True

    column_formatters = {
        'responsibles': user_formatter,
        'delegate': delegate_formatter
    }


# Create customized model view class
class DelegatedUserModelView(AppModelView):
    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True
    column_list = ("roles", "type", "name")
    column_searchable_list = ('type', 'name',)
    can_export = True

    column_formatters = {
        'roles': role_formatter
    }

    def on_model_change(self, form, model, is_created):
        logger.debug(form)
        logger.debug(model)

    def validate_form(self, form):
        logger.debug("Form data: {}".format(form.data))
        ldap_client = CernLdapClient()

        if not form.data.get('type'):
            return True

        if form.data['type'] == DelegatedType.USER:
            logger.debug("Is a user")
            # Get the user from LDAP
            user = ldap_client.get_user_ldap(form.data['name'])
            logger.debug(user)
            if user:
                return True

        elif form.data['type'] == DelegatedType.EGROUP:
            # Check if the egroup exist and set it
            egroup = ldap_client.get_egroup_by_name(form.data['name'])
            if egroup:
                return True

        return False
