import enum

from app.common.models import ModelBase
from app.extensions import db

import logging

logger = logging.getLogger(__name__)

responsible_role_location = db.Table('responsible_role_location',
                            db.Column('responsible_user_id', db.Integer, db.ForeignKey('responsible_user.id'),
                                      primary_key=True),
                            db.Column('role_location_id', db.Integer, db.ForeignKey('role_location.id'),
                                      primary_key=True)
                            )

responsible_role_department = db.Table('responsible_role_department',
                                       db.Column('responsible_user_id', db.Integer, db.ForeignKey('responsible_user.id'),
                                                 primary_key=True),
                                       db.Column('role_department_id', db.Integer, db.ForeignKey('role_department.id'),
                                                 primary_key=True)
                                       )


class RoleType(enum.Enum):
    TSO = "TSO"
    DTSO = "DTSO"
    LOC_RESP = "Location Responsible"
    UNSPECIFIED = "UNSPECIFIED"
    DEP_PHONE_MANAGER = "Departmental Phone Manager"


class DelegatedType(enum.Enum):
    USER = "USER"
    EGROUP = "EGROUP"


class DelegatedUser(ModelBase):
    __tablename__ = 'delegated_user'

    type = db.Column(db.Enum(DelegatedType), default=DelegatedType.USER)
    name = db.Column(db.String(255), nullable=False)

    def __str__(self):
        return "{} ({})".format(self.name, self.type.value)


class RoleDepartment(ModelBase):
    """
    Class only used when OFFICE_MODE == False
    """
    __tablename__ = 'role_department'

    role = db.Column(db.Enum(RoleType), default=RoleType.DEP_PHONE_MANAGER)
    department = db.Column(db.String(255), nullable=False)

    responsibles = db.relationship('ResponsibleUser', secondary=responsible_role_department, lazy='subquery',
                                   backref=db.backref('dep_roles', lazy=True))

    delegate_id = db.Column(db.Integer, db.ForeignKey(DelegatedUser.id))
    delegate = db.relationship(DelegatedUser, foreign_keys=delegate_id, backref='dep_roles')

    def __str__(self):
        return "{} - {}".format(self.role.value, self.department)

    def get_responsibles_usernames(self):
        """
        Gets all the usernames of the responsibles in the current instance
        :return: A list of strings
        """
        return [responsible.username for responsible in self.responsibles]


class RoleLocation(ModelBase):
    __tablename__ = 'role_location'

    role = db.Column(db.Enum(RoleType), default=RoleType.UNSPECIFIED)

    building = db.Column(db.String(255), nullable=False)
    floor = db.Column(db.String(255))
    room = db.Column(db.String(255))

    responsibles = db.relationship('ResponsibleUser', secondary=responsible_role_location, lazy='subquery',
                                   backref=db.backref('roles', lazy=True))

    delegate_id = db.Column(db.Integer, db.ForeignKey(DelegatedUser.id))
    delegate = db.relationship(DelegatedUser, foreign_keys=delegate_id, backref='roles')

    def __str__(self):
        return "{} - {}-{}-{}".format(self.role.value, self.building, self.floor, self.room)

    def get_responsibles_usernames(self):
        """
        Gets all the usernames of the responsibles in the current instance
        :return: A list of strings
        """
        return [responsible.username for responsible in self.responsibles]

