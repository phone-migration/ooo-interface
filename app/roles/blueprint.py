from app.common.views.utils import blueprint_factory

roles_blueprint = blueprint_factory('roles_bp', __name__, url_prefix='/')
