from app.extensions import db
from app.roles.models import RoleLocation, RoleDepartment, RoleType, DelegatedUser


class DelegatedUserMapper:

    @classmethod
    def create_delegated_user(cls, name=None, delegated_type=None, commit=False):
        """
        Creates a new DelegatedUser instance
        :param name: The name of the DelegatedUser
        :param delegated_type: (USER|EGROUP)
        :param commit: Whether or not the changes will be commited to the db
        :return: The new created DelegatedUser
        """
        new = DelegatedUser()
        new.name = name
        new.type = delegated_type
        if commit:
            db.session.add(new)
            db.session.commit()
        return new


class RoleLocationMapper:

    @classmethod
    def create_role_location(cls, role=None, building=None, floor=None, room=None, responsibles=None, commit=False):
        """
        Creates a new RoleLocation instance
        :param building: String. A building name
        :param floor: String. A floor name
        :param room: String. A room name
        :param role: RoleType
        :param responsibles: List with all the responsibles of the location
        :param commit: Whether or not the changes will be committed to the db
        :return: The newly created RoleLocation
        """
        new = RoleLocation()
        new.role = role
        new.building = building
        new.floor = floor
        new.room = room
        new.responsibles = responsibles or []
        if commit:
            db.session.add(new)
            db.session.commit()
        return new


class RoleDepartmentMapper:

    @classmethod
    def create_role_department(cls, role=RoleType.DEP_PHONE_MANAGER, department=None, responsibles=None, commit=False):
        """
        Creates a new RoleDepartment instance
        :param role: RoleType
        :param department: Name of the department
        :param responsibles: List with all the responsibles of the Department
        :param commit: Whether or not the changes will be committed to the db
        :return: The newly created RoleDepartment
        """
        new = RoleDepartment()
        new.role = role
        new.department = department
        new.responsibles = responsibles or []
        if commit:
            db.session.add(new)
            db.session.commit()
        return new
