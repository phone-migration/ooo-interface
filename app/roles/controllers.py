import logging
from collections import OrderedDict

from flask import current_app, session
from flask_login import current_user
from sqlalchemy import or_
from sqlalchemy.orm.exc import NoResultFound

from app.locations.constants import purposes
from app.phone_numbers.models import PhoneNumber, ResponsibleUser
from app.roles.models import RoleLocation, DelegatedUser, RoleDepartment, DelegatedType
from app.services.cern_ldap import CernLdapClient

logger = logging.getLogger(__name__)


def get_phones_from_department_role(department):
    return PhoneNumber.query.filter_by(department=department).all()


def get_phones_from_department(username):
    logger.info("Getting phones from department for user: {}".format(username))
    phones = []

    # We retrieve all the department roles from the user.
    # The current user can be a responsible or a delegate
    responsible_user = None
    try:
        responsible_user = ResponsibleUser.query.filter_by(username=username).one()
    except NoResultFound:
        logger.debug("User is not responsible")
    try:
        responsible_user = DelegatedUser.query.filter_by(name=username).one()
    except NoResultFound:
        logger.debug("User is not delegated")

    ldap_client = CernLdapClient()
    egroups = ldap_client.get_user_egroups_by_username(username)
    roles = get_office_mode_roles(egroups, username)

    if not responsible_user:
        dep_roles = []
    else:
        dep_roles = responsible_user.dep_roles
    roles = roles + dep_roles

    for role in roles:
        phones += get_phones_from_department_role(role.department)

    phones = set(phones)

    logger.info("Found {} phones for user {} departments".format(len(phones), username))

    return phones


def get_all_locations_by_user(username=None):
    """
    Gets all the buildings visible for the user
    :return: A list of Building
    """
    if current_app.config["OFFICE_MODE"]:
        buildings, _ = get_buildings_from_departments(username=username)
    else:
        buildings, _ = get_buildings_from_locations(username=username)

    return buildings

def get_all_phones_by_user(username=None):
    """
    Gets all the buildings visible for the user
    :return: A list of Building
    """
    if current_app.config["OFFICE_MODE"]:
        _, phones = get_buildings_from_departments(username)
    else:
        _, phones = get_buildings_from_locations(username)

    return phones


def get_user_roles(username=None):
    if not username:
        username = current_user.username
    """
    We retrieve all user's roles
    """
    try:
        delegate = DelegatedUser.query.filter_by(name=username, type=DelegatedType.USER).one()
    except NoResultFound:
        delegate = None
    try:
        responsible = ResponsibleUser.query.filter_by(username=username).one()
    except NoResultFound:
        responsible = None
    return delegate, responsible


def get_buildings_from_locations(username=None):

    if current_user and current_user.is_admin and not session.get("impersonate", None):
        phones = PhoneNumber.query.all()
    else:
        buildings, floors, rooms = get_all_user_locations(username=username)
        phones = get_user_phones_from_locations(buildings, floors, rooms)

    buildings = get_all_phones_data(phones)
    return buildings, phones


def get_all_user_locations(username=None):

    if username is None:
        username = current_user.username

    ldap_client = CernLdapClient()
    egroups = ldap_client.get_user_egroups_by_username(username)

    buildings = []
    floors = []
    rooms = []

    roles = get_outside_roles(egroups, username)

    # Buildings
    for role in roles:
        if not role.floor and role.building not in buildings:
            buildings.append(role.building)

    # Floors
    for role in roles:
        # logger.debug(role)
        if role.floor and not role.room and role.building not in buildings:
            combined = "{}-{}".format(role.building, role.floor)
            if combined not in floors:
                floors.append(combined)

    # Rooms
    for role in roles:
        # logger.debug(role)
        if role.room and role.floor and role.building and role.building not in buildings and role.floor not in floors:
            combined = "{}-{}-{}".format(role.building, role.floor, role.room)
            if combined not in rooms:
                rooms.append(combined)


    # logger.debug("Buildings: ")
    # logger.debug(sorted(buildings))
    # logger.debug("Floors: ")
    # logger.debug(sorted(floors))
    # logger.debug("Rooms: ")
    # logger.debug(sorted(rooms))
    return buildings, floors, rooms


def get_user_phones_from_locations(buildings, floors, rooms):
    # logger.debug("Building phones: ")
    phones = PhoneNumber.query.filter(PhoneNumber.building.in_(buildings)).all()
    floors_phones_query = PhoneNumber.query
    if len(floors) > 0:
        for floor in floors:
            floors_phones_query = floors_phones_query.filter_by(building=floor.split('-')[0],
                                                                floor=floor.split('-')[1])
        phones += floors_phones_query.all()
        # logger.debug(phones)
    # logger.debug("Room phones: ")
    if len(rooms) > 0:
        room_phones_query = PhoneNumber.query
        for room in rooms:
            # logger.debug(room)
            room_phones_query = room_phones_query.filter_by(building=room.split('-')[0],
                                                            floor=room.split('-')[1],
                                                            room=room.split('-')[2])
        phones += room_phones_query.all()
    return phones


def get_buildings_from_departments(username=None):
    if current_user and current_user.is_admin and not session.get("impersonate", None):
        phones = PhoneNumber.query.all()
    else:
        phones = get_user_phones_from_departments(username=username)
    buildings = get_all_phones_data(phones)
    return buildings, phones


def get_user_phones_from_departments(username=None):

    if username is None:
        username = current_user.username

    ldap_client = CernLdapClient()
    egroups = ldap_client.get_user_egroups_by_username(username)
    roles = get_office_mode_roles(egroups, username)
    departments = []
    for role in roles:
        logger.debug(role)
        departments.append(role.department)
    phones = PhoneNumber.query.filter(PhoneNumber.department.in_(departments)).all()
    return phones


def get_all_phones_data(phones):
    buildings = {}
    for phone in phones:
        # Building
        append_building_to_buildings(buildings, phone)

        # Floors
        append_floor_to_building(buildings, phone)

        # Rooms
        append_room_to_buildings(buildings, phone)

        try:
            purpose_desc = purposes[phone.purpose]
        except KeyError:
            purpose_desc = ""

        # Phones
        buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]['phones'][phone.phone_number] = {
            'total_calls': phone.total_calls,
            'display_name': phone.display_name,
            'action_to_take': phone.action_to_take.name if phone.action_to_take else '',
            'want_to_keep': phone.want_to_keep,
            'is_personal': phone.is_personal,
            'is_present': phone.is_present,
            'completed': phone.completed,
            'processed': phone.processed,
            'has_ip_socket': phone.has_ip_socket,
            'room': phone.room,
            'floor': phone.floor,
            'building': phone.building,
            'id': phone.id,
            'purpose': phone.purpose,
            'purpose_desc': purpose_desc,
            'owner': phone.owner.to_json() if phone.owner else ''
        }
    buildings = OrderedDict(sorted(buildings.items()))
    return buildings


def append_building_to_buildings(buildings, phone):
    try:
        buildings[phone.building]["total"] += 1
        if phone.processed:
            buildings[phone.building]["processed"] += 1
        if phone.completed:
            buildings[phone.building]["completed"] += 1
    except KeyError:
        buildings[phone.building] = {"floors": {}}
        buildings[phone.building]["total"] = 1
        buildings[phone.building]["processed"] = 0
        buildings[phone.building]["completed"] = 0
        if phone.processed:
            buildings[phone.building]["processed"] = 1
        if phone.completed:
            buildings[phone.building]["completed"] = 1


def append_room_to_buildings(buildings, phone):
    try:
        buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]["total"] += 1
        if phone.processed:
            buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]["processed"] += 1
        if phone.completed:
            buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]["completed"] += 1
    except KeyError:
        buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room] = {'phones': {}}
        buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]['total'] = 1
        buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]['processed'] = 0
        buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]['completed'] = 0
        if phone.processed:
            buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]['processed'] = 1
        if phone.completed:
            buildings[phone.building]['floors'][phone.floor]['rooms'][phone.room]['completed'] = 1


def append_floor_to_building(buildings, phone):
    """
    Retrieves all the location info related to a phone and appends it to the buildings dict
    :param buildings: Dict of buildings
    :param phone: Phone to retrieve the information from
    :return:
    """
    try:
        buildings[phone.building]['floors'][phone.floor]["total"] += 1
        if phone.processed:
            buildings[phone.building]['floors'][phone.floor]["processed"] += 1
        if phone.completed:
            buildings[phone.building]['floors'][phone.floor]["completed"] += 1
    except KeyError:
        buildings[phone.building]['floors'][phone.floor] = {'rooms': {}}
        buildings[phone.building]['floors'][phone.floor]['total'] = 1
        buildings[phone.building]['floors'][phone.floor]['processed'] = 0
        buildings[phone.building]['floors'][phone.floor]['completed'] = 0
        if phone.processed:
            buildings[phone.building]['floors'][phone.floor]['processed'] = 1
        if phone.completed:
            buildings[phone.building]['floors'][phone.floor]['completed'] = 1


def get_outside_roles(egroups, username):
    """
    Gets all the roles for the selected username or any of the egroups.

    :param egroups: List of egroups the user is in
    :param username: Username of the user
    :return: A List of RoleLocation
    """
    query = RoleLocation.query.filter(
        or_(
            RoleLocation.responsibles.any(ResponsibleUser.username == username),
            RoleLocation.delegate.has(name=username),
        )
    ).union(
        RoleLocation.query.join(RoleLocation.delegate, aliased=True).filter(DelegatedUser.name.in_(egroups))
    )
    return query.all()


def get_office_mode_roles(egroups, username):
    """
    Gets all the roles for the selected username or any of the egroups.

    :param egroups: List of egroups the user is in
    :param username: Username of the user
    :return: A List of RoleDepartment
    """
    # logger.debug(egroups)
    query = RoleDepartment.query.filter(
        or_(
            RoleDepartment.responsibles.any(ResponsibleUser.username == username),
            RoleDepartment.delegate.has(name=username),
        )
    ).union(
        RoleDepartment.query.join(RoleDepartment.delegate, aliased=True).filter(DelegatedUser.name.in_(egroups))
    )
    return query.all()
